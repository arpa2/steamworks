# This is SteamWorks, a distribution and transformation system for LDAP data

#
#   SPDX-FileCopyrightText: 2014, 2015 InternetWide.org and the ARPA2.net project
#   SPDX-FileCopyrightText: 2017, Adriaan de Groot <groot@kde.org>
#   SPDX-FileCopyrightText: 2019,2020 Adriaan de Groot <groot@kde.org>
#   SPDX-License-Identifier: BSD-2-Clause
#

cmake_minimum_required(VERSION 3.13 FATAL_ERROR)
project(SteamWorks VERSION 0.97.3 LANGUAGES C CXX)

# Do not dereference quoted strings in if()
cmake_policy(SET CMP0054 NEW)

set(CMAKE_C_STANDARD 99)
set(CMAKE_C_STANDARD_REQUIRED ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include (FeatureSummary)

find_package (ARPA2CM 0.9.0 QUIET NO_MODULE)
set_package_properties (ARPA2CM PROPERTIES
    DESCRIPTION "CMake modules for ARPA2 projects"
    TYPE REQUIRED
    URL "https://gitlab.com/arpa2/arpa2cm/"
    PURPOSE "Required for the CMake build system for ${PROJECT}"
)
if (ARPA2CM_FOUND)
    set (CMAKE_MODULE_PATH
        ${CMAKE_MODULE_PATH}
        ${ARPA2CM_MODULE_PATH}
        ${CMAKE_SOURCE_DIR}/cmake)
else()
    feature_summary (WHAT ALL)
    message (FATAL_ERROR "ARPA2CM is required.")
endif()

include(MacroAddUninstallTarget)
include(MacroCreateConfigFiles)
include(MacroEnsureOutOfSourceBuild)
include(MacroGitVersionInfo)

macro_ensure_out_of_source_build("Do not build SteamWorks in the source directory.")
get_project_git_version()

### DEPENDENCIES
#
#
find_package(Log4cpp)
set_package_properties (Log4cpp PROPERTIES
    DESCRIPTION "Log4cpp logging framework"
    TYPE OPTIONAL
    URL "http://log4cpp.sourceforge.net/"
    PURPOSE "Logging of requests"
)
if(NOT Log4cpp_FOUND)
  message(STATUS "Disable logging ...")
  add_definitions(-DNDEBUG)
endif()

find_package(OpenLDAP)
set_package_properties (OpenLDAP PROPERTIES
    DESCRIPTION "OpenLDAP"
    TYPE REQUIRED
    URL "https://www.openldap.org/"
    PURPOSE "LDAP access library"
)

find_package(Catch2)
set_package_properties(Catch2 PROPERTIES
    DESCRIPTION "Catch2"
    TYPE OPTIONAL
    URL "https://github.com/catchorg/Catch2"
    PURPOSE "Additional tests"
)

feature_summary(FATAL_ON_MISSING_REQUIRED_PACKAGES WHAT REQUIRED_PACKAGES_NOT_FOUND)


### BUILD
#
#
enable_testing()
if(Catch2_FOUND)
    include(CTest)
    include(Catch)
endif()

include_directories(${CMAKE_SOURCE_DIR}/src)

add_subdirectory(src/3rdparty/fcgi-2.4.0)
add_subdirectory(src/common)
add_subdirectory(src/swldap)
add_subdirectory(src/crank)
add_subdirectory(src/shaft)
add_subdirectory(src/pulley)

add_uninstall_target()

create_config_files(SteamWorks)
install(FILES cmake/SteamWorksPulleyback.cmake DESTINATION ${SteamWorks_DIR})

# Ensure that the build directory has a useful logging configuration,
# in case you want to run tests there.
set(_target_log_configs crank.properties shaft.properties pulley.properties)
set(_source_log_config ${CMAKE_SOURCE_DIR}/docs/steamworks.properties)
if(NOT EXISTS ${_source_log_config})
    message(FATAL_ERROR "The logging configuration file '${_source_log_config}' is missing.")
endif()
foreach(_f ${_target_log_configs})
    if(${_source_log_config} IS_NEWER_THAN ${CMAKE_BINARY_DIR}/${_f})
        configure_file(${_source_log_config} ${CMAKE_BINARY_DIR}/${_f} COPYONLY)
    endif()
endforeach()


### PACKAGING
#
#
set(CPACK_PACKAGE_NAME "SteamWorks")
set(CPACK_PACKAGE_VERSION ${SteamWorks_VERSION})
set(CPACK_PACKAGE_VENDOR "ARPA2.net")
set(CPACK_PACKAGE_CONTACT "Adriaan de Groot <groot@kde.org>")
set(CPACK_FREEBSD_PACKAGE_LICENSE "BSD-2-Clause")

include(PackAllPossible)
include(CPack)

feature_summary(FATAL_ON_MISSING_REQUIRED_PACKAGES WHAT ALL)
