# Build a SteamWorks Pulley Backend (Plugin)
#
# A SteamWorks Pulley Backend is a shared library that can be loaded
# by the Pulley, on-demand when a corresponding Pulley script is
# loaded. The backend uses the SteamWorks Pulley Backend API and
# must implement the Pulley Pulley Backend Interface functions,
# which will be called by the Pulley to do work.
#
#
# steamworks_pulleyback(
#   name
#   SOURCES file ...
#   [LIBRARIES lib ...]
#   [ENABLE_STATIC]
# )
#
# This will build a module called *pulleyback_<name>* which is
# installed to the SteamWorks Pulley Backends directory.
# The module links to additional (optional) libraries that
# are listed; note it does **not** link to SteamWorks directly,
# since the API is resolved and injected at runtime.
#
# If ENABLE_STATIC is given, then an (additional) static library
# is built with the same sources. The static library is called
# *pulleyback_<name>_a* and is not installed. This can be useful
# for tests.
#

#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2020, Adriaan de Groot <groot@kde.org>
#

function(steamworks_pulleyback _name)
    set(NAME ${_name})
    cmake_parse_arguments(_sp "ENABLE_STATIC" "" "SOURCES;LIBRARIES" ${ARGN})

    if(NOT _sp_NAME)
        set(_sp_NAME ${NAME})
    endif()
    set(_name pulleyback_${_sp_NAME})

    if(_sp_ENABLE_STATIC)
        # This probably won't work because of -fPIC issues
        add_library(${_name}_a STATIC ${_sp_SOURCES})
        target_link_libraries(${_name}_a ${_sp_LIBRARIES})
    endif()

    # .. create the shared (plugin) library
    add_library(${_name} MODULE ${_sp_SOURCES})
    target_link_libraries(${_name} ${_sp_LIBRARIES})
    set_target_properties(${_name}
        PROPERTIES
            PREFIX ""
            INSTALL_RPATH_USE_LINK_PATH TRUE
    )

    install (TARGETS ${_name}
        LIBRARY DESTINATION ${SteamWorks_PULLEY_DIR}
    )
endfunction()
