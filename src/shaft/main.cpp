/*
 *   SPDX-FileCopyrightText: 2014-2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#include "shaft.h"

#include "common/fcgi.h"
#include "common/logger.h"

static const char* copyright = "Copyright (C) 2014-2016 InternetWide.org and the ARPA2.net project";

int main(int argc, char** argv)
{
	SteamWorks::Logging::Manager logManager("shaft.properties");
	SteamWorks::Logging::getRoot().debugStream() << "SteamWorks Shaft " << copyright;

	ShaftDispatcher* dispatcher = new ShaftDispatcher();
	SteamWorks::FCGI::init_logging("steamworks.shaft.fcgi");
	SteamWorks::FCGI::mainloop(dispatcher);

	return 0;
}
