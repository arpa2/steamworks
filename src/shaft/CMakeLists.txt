# SPDX-FileCopyrightText: 2016 InternetWide.org and the ARPA2.net project
# SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
# SPDX-License-Identifier: BSD-2-Clause

add_executable(shaft
  main.cpp
  shaft.cpp
)
target_link_libraries(shaft swldap swcommon)
set_property(TARGET shaft PROPERTY RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})

install(TARGETS shaft
  RUNTIME DESTINATION bin
)
