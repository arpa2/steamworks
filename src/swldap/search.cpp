/*
 *   SPDX-FileCopyrightText: 2014-2015 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#include "search.h"

#include "private.h"

#include "3rdparty/picojson.h"

static SteamWorks::Logging::Logger& myLogger()
{
	return SteamWorks::Logging::getLogger("steamworks.ldap");
}

/**
 * Internals of a search. A search holds a base dn for the search
 * and a filter expression. When executed, it returns an array
 * of objects found.
 */
class SteamWorks::LDAP::Search::Private
{
private:
	std::string m_base, m_filter;
	LDAPScope m_scope;

public:
	Private(const std::string& base, const std::string& filter, LDAPScope scope)
	    : m_base(base)
	    , m_filter(filter)
	    , m_scope(scope)
	{
	}

	const std::string& base() const { return m_base; }
	const std::string& filter() const { return m_filter; }
	LDAPScope scope() const { return m_scope; }
};

SteamWorks::LDAP::Search::Search(const std::string& base, const std::string& filter, LDAPScope scope)
    : Action(true)
    , d(new Private(base, filter, scope))
{
}

SteamWorks::LDAP::Search::~Search() {}

void SteamWorks::LDAP::Search::execute(Connection& conn, Result result)
{
	::LDAP* ldaphandle = handle(conn);

	auto& log = myLogger();

	// TODO: settings for timeouts?
	struct timeval tv;
	tv.tv_sec = 2;
	tv.tv_usec = 0;

	LDAPMessage* res;
	int r = ldap_search_ext_s(ldaphandle,
				  d->base().c_str(),
				  d->scope(),
				  d->filter().c_str(),
				  nullptr,  // attrs
				  0,
				  server_controls(conn),
				  client_controls(conn),
				  &tv,
				  1024 * 1024,
				  &res);
	if (r)
	{
		log.errorStream() << "Search result " << r << " " << ldap_err2string(r);
		ldap_msgfree(res);  // Should be freed regardless of the return value
		return;
	}

	copy_search_result(ldaphandle, res, result, log);

	ldap_msgfree(res);
}

/** Internals of a Remove (delete) action.
 */
class SteamWorks::LDAP::Remove::Private
{
private:
	std::string m_dn;

public:
	Private(const std::string& dn)
	    : m_dn(dn) {};

	const std::string& name() const { return m_dn; }
};

SteamWorks::LDAP::Remove::Remove(const std::string& dn)
    : Action(!dn.empty())
    , d(new Private(dn))
{
}

SteamWorks::LDAP::Remove::~Remove() {}

void SteamWorks::LDAP::Remove::execute(Connection& conn, Result result)
{
	auto& log = myLogger();
	if (!is_valid())
	{
		log.warnStream() << "Can't execute invalid removal.";
		return;
	}

	log.debugStream() << "Removal execute:" << d->name();

	int r = ldap_delete_ext_s(handle(conn), d->name().c_str(), server_controls(conn), client_controls(conn));
	log.debugStream() << "Result " << r << " " << (r ? ldap_err2string(r) : "OK");
}
