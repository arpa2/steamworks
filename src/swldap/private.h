/*
 *   SPDX-FileCopyrightText: 2014-2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

/**
 * Private implementation details, also support functions.
 */

#ifndef SWLDAP_PRIVATE_H
#define SWLDAP_PRIVATE_H

#include "connection.h"

#include "3rdparty/picojson.h"
#include "common/logger.h"

#include <ldap.h>

#include <string>

namespace SteamWorks
{

namespace LDAP
{

/**
 * Copy the DIT entry contained in @p entry into the JSON object @p results,
 * with attribute names as keys in @p results (e.g. "dn" is a key) and
 * null, string or array values for each attribute.
 */
void copy_entry(::LDAP* ldaphandle, ::LDAPMessage* entry, Result results);
/**
 * Copy the DIT entries contained in @p res into the JSON object @p results,
 * logging to @p log as needed. The DN of each entry is used as key in
 * @p results, and each value is a JSON object filled as described in
 * copy_entry().
 */
void copy_search_result(::LDAP* ldaphandle, ::LDAPMessage* res, Result results, Logging::Logger& log);

/** @brief Wrapper around ldap_get_dn() that automatically frees memory
 *
 * Create one of these to create std::string for the DN of an
 * LDAP message (common antipattern: std::string s(ldap_get_dn(...))
 * leaks memory)
 */
class GetDN
{
public:
	GetDN(::LDAP* ldap, ::LDAPMessage* msg)
	    : p(ldap_get_dn(ldap, msg))
	{
	}
	~GetDN() { ::ldap_memfree((void*)p); }

	/// Use when the DN is temporary and you just want the string (e.g. logging)
	std::string str() const { return std::string(p); }

	/// Conversion if you want to cast the whole thing to std::string
	operator std::string() const { return std::string(p); }

	/// Cases where you need the raw pointer (e.g. other LDAP functions)
	const char* ptr() const { return p; }

private:
	const char* p = nullptr;
};

/** @brief Logging an exploded DN
 *
 * For now, this is a debugging interface.
 */
void explode_dn(::LDAP* ldaphandle, ::LDAPMessage* entry);
void explode_dn(const GetDN& dn);

}  // namespace LDAP
}  // namespace SteamWorks

#endif
