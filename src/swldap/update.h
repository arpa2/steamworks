/*
 *   SPDX-FileCopyrightText: 2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

/**
 * LDAP operations in a C++ jacket.
 *
 * Update a single DIT entry, with or without precondition-checking,
 * with or without automatic rollback, and optionally with explicit
 * rollback.
 */

#ifndef SWLDAP_UPDATE_H
#define SWLDAP_UPDATE_H

#include "action.h"

#include <string>

#include <variant>

namespace SteamWorks
{

namespace LDAP
{

/**
 * (Synchronous) update. Updates change zero
 * or more attributes of a given dn.
 */
class Update : public Action
{
	friend class Addition;

private:
	class Private;
	std::unique_ptr<Private> d;

public:
	// 0, 1 or more values for the attribute;
	using NoValue = std::monostate;
	using OneValue = std::string;
	using MultiValue = std::vector<std::string>;
	using AttributeValue = std::variant<NoValue, OneValue, MultiValue>;
	using Attributes = std::map<std::string, AttributeValue>;

	Update(const std::string& dn);  // Empty update (not valid)
	Update(const std::string& dn, const Attributes& attr);  // Update one entry (dn)
	Update(const picojson::value& json);  // Update multiple entries (dn's)
	~Update();

	virtual void execute(Connection&, Result = nullptr) override;
};

/**
 * (Synchronous) addition. Like an update,
 * but adds a new entry to the DIT with an
 * all-new DN.
 */
class Addition : public Update
{
public:
	Addition(const std::string& dn, const Attributes& attr);
	Addition(const picojson::value& v);

	virtual void execute(Connection&, Result result = nullptr) override;
};

}  // namespace LDAP
}  // namespace SteamWorks

#endif
