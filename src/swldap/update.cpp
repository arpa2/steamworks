/*
 *   SPDX-FileCopyrightText: 2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#include "update.h"

#include "private.h"

#include "3rdparty/picojson.h"

static SteamWorks::Logging::Logger& myLogger()
{
	return SteamWorks::Logging::getLogger("steamworks.ldap");
}

static inline bool is_postcondition(const std::string& attr)
{
	return attr[0] == '>';
}

static inline bool is_precondition(const std::string& attr)
{
	return attr[0] == '<';
}

static inline bool is_precondition(const SteamWorks::LDAP::Update::Attributes::const_iterator& it)
{
	return is_precondition(it->first);
}

/**
 * Internals of an update.
 */
class SteamWorks::LDAP::Update::Private
{
private:
	std::string m_dn;
	SteamWorks::LDAP::Update::Attributes m_map;

public:
	Private(const std::string& dn)
	    : m_dn(dn) {};
	void update(const SteamWorks::LDAP::Update::Attributes& attrs) { m_map.insert(attrs.cbegin(), attrs.cend()); }
	void update(const std::string& name, const std::string& value) { m_map.emplace(name, value); }
	void remove(const std::string& name) { m_map.erase(name); }

	size_t size() const { return m_map.size(); }

	const std::string& name() const { return m_dn; }

	SteamWorks::LDAP::Update::Attributes::const_iterator begin() const { return m_map.begin(); }

	SteamWorks::LDAP::Update::Attributes::const_iterator end() const { return m_map.end(); }
};

class LDAPMods
{
public:
	using U = SteamWorks::LDAP::Update;

private:
	::ldapmod** m_mods;
	size_t m_size;
	unsigned int m_count;

	/** @brief Adds another modify-structure to this modification
	 *
	 * Returns a pointer to the modify-structure, or nullptr if
	 * none can be added.
	 */
	::ldapmod* get_mod(int mod_op)
	{
		::ldapmod* mod = nullptr;

		if (m_count < m_size)
		{
			// TODO: check for allocation failures
			mod = m_mods[m_count++] = (::ldapmod*)malloc(sizeof(::ldapmod));
		}
		if (mod)
		{
			mod->mod_op = mod_op;
		}
		return mod;
	}

	void set_mod_attributes(::ldapmod* mod, const std::string& attr, const U::AttributeValue& value)
	{
		if (!mod)
		{
			return;
		}
		switch (value.index())
		{
		case 1:
		{
			const auto& val = std::get<U::OneValue>(value);
			mod->mod_type = const_cast<char*>(attr.c_str());
			mod->mod_vals.modv_strvals = (char**)calloc(2, sizeof(char*));
			mod->mod_vals.modv_strvals[0] = const_cast<char*>(val.c_str());
			mod->mod_vals.modv_strvals[1] = nullptr;
		}
		break;
		case 2:
		{
			const auto& values = std::get<U::MultiValue>(value);
			mod->mod_type = const_cast<char*>(attr.c_str());
			mod->mod_vals.modv_strvals = (char**)calloc(values.size() + 1, sizeof(char*));

			int index = 0;
			for (const auto& val : values)
			{
				mod->mod_vals.modv_strvals[index++] = const_cast<char*>(val.c_str());
			}
			mod->mod_vals.modv_strvals[index++] = nullptr;
		}
		break;
		case 0:
		default:
			break;
		}
		if (mod && (is_precondition(attr) || is_postcondition(attr)))
		{
			// The pointer points into data in @p attr and is not freed,
			// so we'll bump it past the <
			mod->mod_type++;
		}
	}

public:
	LDAPMods(size_t n)
	    : m_mods((::ldapmod**)calloc(n + 1, sizeof(::ldapmod*)))
	    , m_size(n)
	    , m_count(0)
	{
	}

	~LDAPMods()
	{
		for (unsigned int i = 0; i < m_count; i++)
		{
			free(m_mods[i]->mod_vals.modv_strvals);
			free(m_mods[i]);
		}
		free(m_mods);
		m_mods = nullptr;
	}

	void replace(const std::string& attr, const U::AttributeValue& value)
	{
		::ldapmod* mod = get_mod(LDAP_MOD_REPLACE);
		set_mod_attributes(mod, attr, value);
	}

	/** @brief Remove an entire attribute
	 *
	 * The named attribute @p attr is removed entirely from the LDAP entry
	 */
	void remove(const std::string& attr)
	{
		::ldapmod* mod = get_mod(LDAP_MOD_DELETE);
		if (mod)
		{
			mod->mod_type = const_cast<char*>(attr.c_str());
			mod->mod_values = nullptr;
		}
	}

	/** @brief Remove a specific value from an attribute
	 *
	 * The named attribute @p attr has the specific @p value removed;
	 * a multi-valued attribute will have that removed from the list,
	 * while a single-valued attribute will be cleared.
	 *
	 * The operation will **fail** if the value is not matched.
	 */
	void remove(const std::string& attr, const U::AttributeValue& value)
	{
		::ldapmod* mod = get_mod(LDAP_MOD_DELETE);
		set_mod_attributes(mod, attr, value);
	}

	::ldapmod** c_ptr() const { return m_mods; }
};

SteamWorks::LDAP::Update::Update(const std::string& dn)
    : Action(false)  // invalid
    , d(new Private(dn))
{
}

SteamWorks::LDAP::Update::Update(const std::string& dn, const SteamWorks::LDAP::Update::Attributes& attr)
    : Action(attr.size() > 0)  // valid if we do *something*
    , d(new Private(dn))
{
	d->update(attr);
}

SteamWorks::LDAP::Update::Update(const picojson::value& json)
    : Action(false)  // invalid until we find something in the json
    , d(nullptr)
{
	if (!json.is<picojson::value::object>())
	{
		return;
	}

	std::string dn = json.get("dn").to_str();
	if (!dn.empty())
	{
		d.reset(new Private(dn));
	}
	else
	{
		return;  // no dn? remain invalid
	}

	const picojson::object& o = json.get<picojson::object>();
	if (o.size() > 1)  // "dn" plus one more
	{
		for (const auto& i : o)
		{
			if (i.first != "dn")
			{
				d->update(i.first, i.second.to_str());
			}
		}
		m_valid = true;
	}
}

SteamWorks::LDAP::Update::~Update() {}

void SteamWorks::LDAP::Update::execute(Connection& conn, Result)
{
	auto& log = myLogger();
	if (!m_valid)
	{
		log.warnStream() << "Can't execute invalid update.";
		return;
	}

	log.debugStream() << "Update execute:" << d->name() << " #changes:" << d->size();

	LDAPMods mods(d->size());
	// Do preconditions first
	for (auto i = d->begin(); i != d->end(); ++i)
	{
		if (is_precondition(i))
		{
			mods.remove(i->first, i->second);
		}
	}

	// Do non-preconditions
	for (auto i = d->begin(); i != d->end(); ++i)
	{
		if (!is_precondition(i))
		{
			mods.replace(i->first, i->second);
		}
	}

	auto* ldap_mods = mods.c_ptr();
	for (size_t i = 0; i < d->size(); ++i)
	{
		::ldapmod* mod = ldap_mods[i];
		myLogger().warnStream() << "OP=" << (mod ? mod->mod_op : -1)
					<< " attr=" << (mod ? mod->mod_type : "(null)") << " firstval="
					<< (mod && mod->mod_vals.modv_strvals ? mod->mod_vals.modv_strvals[0]
									      : "(null)");
	}
	int r = ldap_modify_ext_s(
	    handle(conn), d->name().c_str(), ldap_mods, server_controls(conn), client_controls(conn));
	log.debugStream() << "Result " << r << " " << (r ? ldap_err2string(r) : "OK");
}


/**
 * Addition as a variation on updates.
 */
SteamWorks::LDAP::Addition::Addition(const picojson::value& v)
    : Update(v)
{
}

SteamWorks::LDAP::Addition::Addition(const std::string& dn, const Attributes& a)
    : Update(dn, a)
{
}

void SteamWorks::LDAP::Addition::execute(Connection& conn, Result result)
{
	auto& log = myLogger();
	if (!m_valid)
	{
		log.warnStream() << "Can't execute invalid addition.";
		return;
	}

	log.debugStream() << "Addition execute:" << d->name() << " #changes:" << d->size();

	// TODO: here we assume each JSON-change maps to one LDAP modification
	LDAPMods mods(d->size());
	for (auto i = d->begin(); i != d->end(); ++i)
	{
		mods.replace(i->first, i->second);
	}

	int r = ldap_add_ext_s(
	    handle(conn), d->name().c_str(), mods.c_ptr(), server_controls(conn), client_controls(conn));
	log.debugStream() << "Result " << r << " " << (r ? ldap_err2string(r) : "OK");
}
