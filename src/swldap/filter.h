/*
 *   SPDX-FileCopyrightText: 2014-2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

/**
 * LDAP operations in a C++ jacket.
 *
 * BaseFilter -- combines a base DN and a filter expression
 */

#ifndef SWLDAP_FILTER_H
#define SWLDAP_FILTER_H

#include <memory>
#include <string>

namespace SteamWorks
{

namespace LDAP
{
/** @brief A base (for query) and filter expression
 */
struct BaseFilter : std::pair<std::string, std::string>
{
	std::string& base() { return first; }
	std::string& filter() { return second; }
};


}  // namespace LDAP
}  // namespace SteamWorks

#endif
