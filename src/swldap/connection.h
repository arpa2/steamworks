/*
 *   SPDX-FileCopyrightText: 2014-2015 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

/**
 * LDAP operations in a C++ jacket.
 *
 * The primary class here is Connection, which represents a connection
 * to an LDAP server. The connection may be invalid. Connections are
 * disconnected when destroyed. Operate on a connection by creating
 * Actions and then executing them on that connection.
 */

#ifndef SWLDAP_CONNECTION_H
#define SWLDAP_CONNECTION_H

#include "common/logger.h"
#include "common/request.h"

#include "3rdparty/picojson.h"

#include <memory>
#include <string>

typedef struct ldap LDAP;
typedef struct ldapcontrol LDAPControl;

namespace SteamWorks
{

namespace LDAP
{
class Action;

using Result_t = picojson::value::object;
using Result = Result_t*;

/**
 * Connection to an LDAP server.
 */
class Connection
{
	friend class Action;

private:
	class Private;
	std::unique_ptr<Private> d;

protected:
	::LDAP* handle() const;
	::LDAPControl** client_controls() const;
	::LDAPControl** server_controls() const;

public:
	/**
	 * Connect to an LDAP server given by the URI.
	 * All connections use TLS, regardless of the protocol
	 * in the URI, so it is preferable to use "ldap:" URIs
	 * over other methods.
	 */
	Connection(const std::string& uri);
	/**
	 * As Connection(uri) above, but also set username and password
	 * for simple authentication.
	 */
	Connection(const std::string& uri, const std::string& user, const std::string& password);
	~Connection();
	bool is_valid() const;
	std::string get_uri() const;
};

using ConnectionUPtr = std::unique_ptr<SteamWorks::LDAP::Connection>;

/**
 * Try to connect to an LDAP server at the given URI.
 * The @p connection is reset regardless; if connection fails,
 * returns false and a response-code and -explanation in @p response.
 */
bool do_connect(ConnectionUPtr& connection, const std::string& uri, JSON::Object& response, Logging::Logger& log);
/**
 * Try to connect to an LDAP server at the given URI and with
 * authorization binddn and secret (e.g. username and password).
 * The @p connection is reset regardless; if connection fails,
 * returns false and a response-code and -explanation in @p response.
 */
bool do_connect(ConnectionUPtr& connection,
		const std::string& uri,
		const std::string& binddn,
		const std::string& secret,
		JSON::Object& response,
		Logging::Logger& log);
/**
 * As above, but take connection information from the @p values,
 * which may contain keys "uri", "user" and "password".
 */
bool do_connect(ConnectionUPtr& connection, const JSON::Values& values, JSON::Object& response, Logging::Logger& log);
/**
 * As above, but returns a new connection, taking information from @p values.
 * The new connection may be nullptr, in which case the connection failed
 * and there is an explanation in @p response.
 */
ConnectionUPtr do_connect(const JSON::Values& values, JSON::Object& response, Logging::Logger& log);

}  // namespace LDAP
}  // namespace SteamWorks

#endif
