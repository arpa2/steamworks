/*
 *   SPDX-FileCopyrightText: 2014-2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#include "private.h"

#include "serverinfo.h"

namespace SteamWorks
{

namespace LDAP
{

void copy_entry(::LDAP* ldaphandle, ::LDAPMessage* entry, Result map)
{
	if (!map)
	{
		return;
	}

	// TODO: guard against memory leaks
	BerElement* berp(nullptr);
	char* attr = ldap_first_attribute(ldaphandle, entry, &berp);

	while (attr != nullptr)
	{
		berval** values = ldap_get_values_len(ldaphandle, entry, attr);
		auto values_len = ldap_count_values_len(values);
		if (values_len == 0)
		{
			// TODO: can this happen?
			picojson::value v_attr;
			map->emplace(attr, v_attr);  // null
		}
		else if (values_len > 1)
		{
			// FIXME: decode ber-values
			picojson::value::array v_array;
			v_array.reserve(values_len);
			for (decltype(values_len) i = 0; i < values_len; i++)
			{
				picojson::value v_attr(values[i]->bv_val);
				v_array.emplace_back(v_attr);
			}
			picojson::value v_attr(v_array);
			map->emplace(attr, v_attr);
		}
		else
		{
			// FIXME: decode ber-values
			picojson::value v_attr(values[0]->bv_val);
			map->emplace(attr, v_attr);
		}
		ldap_value_free_len(values);
		attr = ldap_next_attribute(ldaphandle, entry, berp);
	}

	if (berp)
	{
		ber_free(berp, 0);
	}
}

void copy_search_result(::LDAP* ldaphandle, ::LDAPMessage* res, Result results, SteamWorks::Logging::Logger& log)
{
	log.infoStream() << "Search message count=" << ldap_count_messages(ldaphandle, res);

	auto count = ldap_count_entries(ldaphandle, res);
	log.infoStream() << " .. entries count=" << count;
	if (count)
	{
		LDAPMessage* entry = ldap_first_entry(ldaphandle, res);
		while (entry != nullptr)
		{
			std::string dn(SteamWorks::LDAP::GetDN(ldaphandle, entry));
			log.infoStream() << " .. entry dn=" << dn;

			if (results)
			{
				picojson::value::object object;
				picojson::value v_object(object);
				results->emplace(dn, v_object);

				picojson::value& placed_obj = results->at(dn);
				picojson::value::object& placed_map = placed_obj.get<picojson::value::object>();

				picojson::value v(dn);
				placed_map.emplace(std::string("dn"), v);

				copy_entry(ldaphandle, entry, &placed_map);
			}

			entry = ldap_next_entry(ldaphandle, entry);
		}
	}
	log.infoStream() << " .. search OK.";
}

void explode_dn(::LDAP* ldaphandle, ::LDAPMessage* entry)
{
	explode_dn(GetDN(ldaphandle, entry));
}

void explode_dn(const GetDN& dn)
{
	auto& log = SteamWorks::Logging::getLogger("steamworks.ldap.dn");
	LDAPDN dn_parts = nullptr;  // typedef LDAPRDN* LDAPDN
	int number_of_parts = ldap_str2dn(dn.ptr(), &dn_parts, LDAP_DN_FORMAT_LDAPV3 | LDAP_DN_PEDANTIC);
	if (number_of_parts || !dn_parts)
	{
		log.warnStream() << "DN from " << Logging::LogPointer(dn.ptr()) << " can not be exploded.";
		return;
	}

	// dn_parts points to an array of pointers to RDN structures, which
	// is NULL-terminated. The RDN structures point to an AVA structure.
	for (int i = 0;; ++i)
	{
		auto* this_part = dn_parts[i] ? *dn_parts[i] : nullptr;
		if (this_part)
		{
			log.debugStream()
			    << "DN part#" << i << ' ' << this_part->la_attr.bv_val << '=' << this_part->la_value.bv_val;
		}
		else
		{
			break;
		}
	}

	ldap_dnfree(dn_parts);
}

}  // namespace LDAP
}  // namespace SteamWorks
