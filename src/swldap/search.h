/*
 *   SPDX-FileCopyrightText: 2014-2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

/**
 * LDAP operations in a C++ jacket.
 *
 * Search and Update on an LDAP server. Both actions may be kept around and
 * execute()d one of more times (each time will run the search or query,
 * returning current results as needed).
 */

#ifndef SWLDAP_SEARCH_H
#define SWLDAP_SEARCH_H

#include "action.h"

#include "3rdparty/picojson.h"
#include "common/logger.h"

// For LDAP_SCOPE_SUBTREE
#include <ldap.h>

#include <map>
#include <string>

namespace SteamWorks
{

namespace LDAP
{
using LDAPScope = decltype(LDAP_SCOPE_SUBTREE);  // Probably int

/**
 * (Synchronous) search. The search places results in the result parameter of
 * execute(), in JSON form.
 */
class Search : public Action
{
private:
	class Private;
	std::unique_ptr<Private> d;

public:
	Search(const std::string& base, const std::string& filter, LDAPScope scope = ScopeSubtree);
	~Search();

	virtual void execute(Connection&, Result result = nullptr) override;

	typedef enum
	{
		ScopeSubtree = LDAP_SCOPE_SUBTREE,
		ScopeBase = LDAP_SCOPE_BASE
	} LDAPScope_e;
};

/**
 * (Synchronous) delete. Deletes a single DIT entry
 * identified by a dn.
 */
class Remove : public Action
{
private:
	class Private;
	std::unique_ptr<Private> d;

public:
	Remove(const std::string& dn);
	~Remove();

	virtual void execute(Connection&, Result result = nullptr) override;
};


}  // namespace LDAP
}  // namespace SteamWorks

#endif
