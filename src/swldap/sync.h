/*
 *   SPDX-FileCopyrightText: 2014-2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

/**
 * LDAP operations in a C++ jacket.
 *
 * Sync -- as in SyncRepl according to RFC4533 -- actions on an LDAP connection.
 */

#ifndef SWLDAP_SYNC_H
#define SWLDAP_SYNC_H


#include "action.h"

#include <memory>
#include <string>

namespace SteamWorks
{

namespace LDAP
{

/**
 * (Synchronous) sync.
 */
class SyncRepl : public Action
{
private:
	class Private;
	std::unique_ptr<Private> d;

protected:
	void after_poll();
	virtual void after_modification(const std::string& removed);
	virtual void after_modification(const std::string& modified, const picojson::object& values);

public:
	SyncRepl(const std::string& base, const std::string& filter);
	~SyncRepl();

	std::string base() const;
	std::string filter() const;

	virtual void execute(Connection&, Result result = nullptr) override;
	void poll(Connection&);
	void resync();

	/** Debugging, dump the DIT entries stored in this SyncRepl into @p result */
	void dump_dit(Result result);
};

}  // namespace LDAP
}  // namespace SteamWorks

#endif
