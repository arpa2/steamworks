/*
 *   SPDX-FileCopyrightText: 2014-2015 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

/**
 * LDAP operations in a C++ jacket.
 *
 * The primary class here is Connection, which represents a connection
 * to an LDAP server. The connection may be invalid. Connections are
 * explicitly disconnected when destroyed.
 */

#ifndef SWLDAP_ACTION_H
#define SWLDAP_ACTION_H

#include "swldap/connection.h"

namespace SteamWorks
{

namespace LDAP
{

/**
 * Base class for actions that are created (once) and executed (possibly more
 * than once) against an LDAP connection. Subclasses of Action do
 * actual work. Actions can be created and then executed one or more
 * times; depending on the subclass involved, actions may need
 * additional setup before they become valid to execute.
 */
class Action
{
protected:
	bool m_valid;

	Action(bool a)
	    : m_valid(a)
	{
	}

	::LDAP* handle(Connection& conn) const { return conn.handle(); }
	::LDAPControl** client_controls(Connection& conn) const { return conn.client_controls(); }
	::LDAPControl** server_controls(Connection& conn) const { return conn.server_controls(); }

public:
	Action()
	    : m_valid(false)
	{
	}

	bool is_valid() const { return m_valid; }

	virtual void execute(Connection& conn, Result result = nullptr) = 0;
};


}  // namespace LDAP
}  // namespace SteamWorks

#endif
