<!-- SPDX-FileCopyrightText: 2017 Adriaan de Groot <groot@kde.org>
     SPDX-License-Identifier: BSD-2-Clause
  -->

# Pulley Examples #

This directory contains examples of Pulley backends and some accompanying
PulleyScripts. All of these backends and scripts are built and installed
by default.

## Null Backend ##

This backend just logs everything passed in to it under steamworks.pulleyback.null.
The arguments passed in when opening the backend are logged as well as each tuple
that is passed in from LDAP.
