# SPDX-FileCopyrightText: 2014-2016 InternetWide.org and the ARPA2.net project
# SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
# SPDX-License-Identifier: BSD-2-Clause

add_definitions(-DPREFIX="${CMAKE_INSTALL_PREFIX}")

add_subdirectory(pulleyscript)

### PULLEY
#
#
add_executable(pulley main.cpp pulley.cpp)
target_link_libraries(pulley
  swldap
  swcommon
  pspplib
  pslib
  )

### EXAMPLE PULLEYBACK
#
# We use the tooling that SteamWorks ships for external use;
# when used by third parties the SteamWorksConfig file has already
# set up a suitable PULLEY_DIR.
include(SteamWorksPulleyback)
set(SteamWorks_PULLEY_DIR ${CMAKE_INSTALL_PREFIX}/share/steamworks/pulleyback)
steamworks_pulleyback(null
    SOURCES
        pulleyback/null.c
)

### PULLEYBACK_TEST
#
# pulleyscript is purely a developer tool, allowing load-testing
# of a given pulley backend. It is not installed.
#
add_executable(pulleyscript pulleyback_test.cpp pulley.cpp pulleyscript/squeal.c)
target_compile_definitions(pulleyscript PUBLIC -DALLOW_INSECURE_DB)
target_link_libraries(pulleyscript
  pspplib
  pslib
  swldap
  swcommon
  )

# We need to link both executables such that the plugins can get
# at the write_logger() symbol from the executable -- otherwise
# the plugin can't produce logging output.
#
# Also spit them into the top-level build, instead of hidden in subdirs.
set_target_properties(pulley pulleyscript PROPERTIES
  LINK_FLAGS -rdynamic
  RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}
  )

install(TARGETS pulley pulleyscript
  RUNTIME DESTINATION bin
  )
install(FILES pulleyback.h
  DESTINATION include/steamworks/
  )
install(
  FILES
    examples/null.ply       # Any person
    examples/null-base.ply  # Adds an element to base
  DESTINATION share/steamworks/examples/)
