/*
 * SPDX-FileCopyrightText: 2016 InternetWide.org and the ARPA2.net project
 * SPDX-FileCopyrightText: 2016 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#ifndef STEAMWORKS_PULLEYSCRIPT_GENERATOR_INT_H
#define STEAMWORKS_PULLEYSCRIPT_GENERATOR_INT_H

struct gentab
{
	type_t gentype;
	type_t *vartype, *drvtype;
	struct generator* gens;
	unsigned int allocated_gens;
	unsigned int count_gens;
};

#endif
