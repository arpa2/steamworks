/*
 * SPDX-FileCopyrightText: 2016 InternetWide.org and the ARPA2.net project
 * SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#ifndef STEAMWORKS_PULLEYSCRIPT_DLFUNC_H
#define STEAMWORKS_PULLEYSCRIPT_DLFUNC_H

#ifndef HAVE_DLFUNC
#ifdef __cplusplus
extern "C"
{
#endif
	/* Answer from StackOverflow.com, 1144107. */
	/* copied from FreeBSD, lib/libc/gen/dlfunc.c */
	struct __dlfunc_arg
	{
		int __dlfunc_dummy;
	};
	typedef void (*dlfunc_t)(struct __dlfunc_arg);

	dlfunc_t dlfunc(const void* handle, const void* symbol);
#ifdef __cplusplus
}
#endif
#endif

#endif
