/*
 *   SPDX-FileCopyrightText: 2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#include "parserpp.h"
#include "bindingpp.h"

#include "condition.h"
#include "driver.h"
#include "generator.h"
#include "parser.h"
#include "squeal.h"
#include "variable.h"

#include "common/jsoniterator.h"
#include "common/logger.h"
#include "common/splitstring.h"

#include "swldap/sync.h"

#include <algorithm>
#include <assert.h>
#include <optional>

static SteamWorks::Logging::Logger& myLogger()
{
	return SteamWorks::Logging::getLogger("steamworks.pulley.script");
}

class SquealOpener
{
public:
	struct squeal* m_sql;

	SquealOpener()
	    : m_sql(nullptr)
	{
	}
	SquealOpener(struct parser* prs)
	    : m_sql(nullptr)
	{
		open(prs);
	}
	~SquealOpener()
	{
		if (m_sql)
		{
			squeal_close(m_sql);
		}
	}

	bool open(struct parser* prs)
	{
		auto& log = myLogger();
		hash_t h = prs->scanhash;

		m_sql = squeal_open(prs->scanhash, gentab_count(prs->gentab), drvtab_count(prs->drvtab));
		if (!m_sql)
		{
			log.errorStream() << "Failed to open SQL for " << std::hex << h;
		}
		else
		{
			log.debugStream() << "Opening SQL for " << std::hex << h << "got SQL @" << (void*)m_sql;
		}

		return m_sql != nullptr;
	}

	void close()
	{
		if (m_sql)
		{
			auto& log = myLogger();
			log.debugStream() << "Closing SQL.";

			squeal_close(m_sql);
			// Don't unlink, leave DB for debugging purposes.
			m_sql = nullptr;
		}
	}
};


class SteamWorks::PulleyScript::Parser::Private
{
private:
	struct parser m_prs;
	SquealOpener m_sql;

	using generator_variablenames_t = std::vector<Variable>;
	std::vector<generator_variablenames_t> m_variables_per_generator;

	bool m_valid;
	State m_state;

	std::forward_list<SteamWorks::PulleyScript::BackendParameters> m_backends;

	// Helper in find_subscriptions()
	std::vector<varnum_t> variables_for_generator(gennum_t g);

public:
	Private()
	    : m_valid(false)
	    , m_state(Parser::State::Initial)
	{
		if (pulley_parser_init(&m_prs))
		{
			m_valid = true;
		}
	}
	~Private()
	{
		m_sql.close();

		if (is_valid())
		{
			pulley_parser_cleanup_syntax(&m_prs);
			pulley_parser_cleanup_semantics(&m_prs);
		}
		m_valid = false;
		m_state = Parser::State::Initial;  // Not really
	}

	const struct parser* parser() const { return &m_prs; }

	bool is_valid() const { return m_valid; }

	Parser::State state() const { return m_state; }

	std::string state_string() const
	{
		const char* s = nullptr;

		switch (m_state)
		{
		case State::Initial:
			s = "Initial";
			break;
		case State::Parsing:
			s = "Parsing";
			break;
		case State::Analyzed:
			s = "Analyzed";
			break;
		case State::Ready:
			s = "Ready";
			break;
		case State::Broken:
			s = "Broken";
			break;
		}

		if (!s)
		{
			s = "Unknown";
		}

		return std::string(s);
	}

	bool can_parse() const
	{
		if ((m_state == Parser::State::Initial) || (m_state == Parser::State::Parsing))
		{
			return true;
		}
		else
		{
			auto& log = myLogger();
			log.errorStream() << "Parser cannot parse Script from state " << state_string();
			return false;
		}
	}

	bool can_analyze() const
	{
		if (m_state == Parser::State::Parsing)
		{
			return true;
		}
		else
		{
			auto& log = myLogger();
			log.errorStream() << "Parser cannot do analysis from state " << state_string();
			return false;
		}
	}

	bool can_generate_sql() const
	{
		if (m_state == Parser::State::Analyzed)
		{
			return true;
		}
		else
		{
			auto& log = myLogger();
			log.errorStream() << "Parser cannot generate SQL from state " << state_string();
			return false;
		}
	}

	int read_file(FILE* input)
	{
		if (!can_parse())
		{
			return 1;
		}

		int prsret = pulley_parser_file(&m_prs, input);
		if (prsret || m_prs.status)
		{
			auto& log = myLogger();
			log.errorStream() << "Could not read file input (" << prsret << ',' << m_prs.status << ')';
			return prsret ? prsret : m_prs.status;
		}

		m_state = Parser::State::Parsing;
		return 0;
	}

	int structural_analysis()
	{
		if (!can_analyze())
		{
			return 1;
		}

		pulley_parser_hash(&m_prs);

		int prsret = 0;

		// Use conditions to drive variable partitions;
		// collect shared_varpartitions for them.
		// This enables
		//  - var_vars2partitions()
		//  - var_partitions2vars()
		cndtab_drive_partitions(m_prs.cndtab);
		vartab_collect_varpartitions(m_prs.vartab);

		// Collect the things a driver needs for its work
		drvtab_collect_varpartitions(m_prs.drvtab);
		drvtab_collect_conditions(m_prs.drvtab);
		drvtab_collect_generators(m_prs.drvtab);
		drvtab_collect_cogenerators(m_prs.drvtab);
		drvtab_collect_genvariables(m_prs.drvtab);
		drvtab_collect_guards(m_prs.drvtab);

		// Collect the things a generator needs for its work.
		// This enables
		//  - gen_share_driverouts()	XXX already done
		//  - gen_share_conditions()	XXX never used (yet)
		//  - gen_share_generators()	XXX never used (yet)
		//NONEED// gen_push_driverout() => gen_share_driverouts()
		//NONEED// gen_push_condition() => gen_share_conditions()
		//NONEED// gen_push_generator() => gen_share_generators()

		auto& log = myLogger();
		log.debugStream() << "Drivers: " << drvtab_count(m_prs.drvtab);
		for (unsigned int drvidx = 0; drvidx < drvtab_count(m_prs.drvtab); drvidx++)
		{
			log.debugStream() << "  .. " << drvidx << " name " << drv_get_module(m_prs.drvtab, drvidx);
			varnum_t binding = drv_get_module_parameters(m_prs.drvtab, drvidx);
			if (binding != VARNUM_BAD)
			{
				struct var_value* value = var_share_value(m_prs.vartab, binding);
				std::vector<std::string> expressions;
				decode_parameter_binding(
				    m_prs.vartab, value->typed_blob.ptr, value->typed_blob.len, expressions);

				for (auto s : expressions)
				{
					log.debugStream() << "  .. " << drvidx << " parm " << s;
				}
			}
		}

		m_state = State::Analyzed;
		return prsret;
	}

	int setup_sql()
	{
		if (!can_generate_sql())
		{
			return 1;
		}
		if (!m_sql.open(&m_prs))
		{
			m_state = State::Broken;
			return 1;
		}

		auto& log = myLogger();

		if (squeal_have_tables(m_sql.m_sql, m_prs.gentab, 0) != 0)
		{
			log.errorStream() << "Could not create SQL tables for script.";
			m_state = State::Broken;
			return 1;
		}

		if (squeal_configure(m_sql.m_sql) != 0)
		{
			log.errorStream() << "Could not prepare SQL statements for drv_all.";
			m_state = State::Broken;
			return 1;
		}

		if (squeal_configure_generators(m_sql.m_sql, m_prs.gentab, m_prs.drvtab) != 0)
		{
			log.errorStream() << "Could not configure generator SQL statements.";
			m_state = State::Broken;
			return 1;
		}

		log.debugStream() << "SQL table definitions generated.";

		// This bit copied out of the compiler;
		gennum_t g, gentabcnt = gentab_count(m_prs.gentab);
		bitset_t* drv;
		bitset_iter_t di;
		drvnum_t d;
		for (g = 0; g < gentabcnt; g++)
		{
			drv = gen_share_driverout(m_prs.gentab, g);
			log.debugStream() << "Found " << bitset_count(drv) << " drivers for generator " << g;
			bitset_iterator_init(&di, drv);
			while (bitset_iterator_next_one(&di, NULL))
			{
				d = bitset_iterator_bitnum(&di);
				log.debugStream() << "Generating for generator " << g << ", driver " << d;

				// TODO: is there anything to do here?
			}
		}

		// m_sql.close();
		m_state = State::Ready;
		return 0;
	}

	// Extract filter-expressions
	std::forward_list<LDAP::BaseFilter> find_subscriptions();
	ExpectedActualCount find_backends(const char* libdir);

	// Remove an entry from the middle-end (post-SQL)
	void remove_entry(const std::string& uuid);
	// Adds / modified an entry, return @c true if something happened
	bool add_entry(const std::string& uuid, const picojson::object& data);

	std::weak_ptr<SteamWorks::PulleyScript::BackendTransaction> m_transaction;
	std::shared_ptr<SteamWorks::PulleyScript::BackendTransaction> begin()
	{
		auto p = m_transaction.lock();
		if (!p)
		{
			p = std::make_shared<SteamWorks::PulleyScript::BackendTransaction>(this);
			m_transaction = p;
		}
		return p;
	}
	void commit();

	const generator_variablenames_t& variable_names(gennum_t generator)
	{
		return m_variables_per_generator.at(generator);
	}
};

SteamWorks::PulleyScript::BackendTransaction::BackendTransaction(SteamWorks::PulleyScript::Parser::Private* parent)
    : m_instance_number(instance_count++)
    , m_parent(parent)
{
	auto& log = SteamWorks::Logging::getLogger(
	    "steamworks.pulley");  // A pulley transaction is happening, not a parser event
	log.debugStream() << "Created transaction:" << m_instance_number;
}

SteamWorks::PulleyScript::BackendTransaction::~BackendTransaction()
{
	auto& log = SteamWorks::Logging::getLogger(
	    "steamworks.pulley");  // A pulley transaction is committed, not a parser event
	if (m_dirty)
	{
		log.debugStream() << "Committing transaction:" << m_instance_number;
		m_parent->commit();
	}
	else
	{
		log.debugStream() << "Cancel empty transaction:" << m_instance_number;
	}
}

unsigned int SteamWorks::PulleyScript::BackendTransaction::instance_count = 0;

SteamWorks::PulleyScript::Parser::Parser()
    : d(new Private)
{
}

SteamWorks::PulleyScript::Parser::~Parser() {}

SteamWorks::PulleyScript::Parser::State SteamWorks::PulleyScript::Parser::state() const
{
	return d->state();
}

std::string SteamWorks::PulleyScript::Parser::state_string() const
{
	return d->state_string();
}

int SteamWorks::PulleyScript::Parser::read_file(const char* filename)
{
	if (!d->can_parse())
	{
		return 1;
	}

	auto& log = myLogger();
	FILE* fh = fopen(filename, "r");
	if (!fh)
	{
		log.errorStream() << "Failed to open " << filename;
		return 1;
	}
	log.debugStream() << "Loading " << filename;

	int r = read_file(fh);
	fclose(fh);
	return r;
}

int SteamWorks::PulleyScript::Parser::read_file(FILE* input)
{
	return d->read_file(input);
}

int SteamWorks::PulleyScript::Parser::structural_analysis()
{
	if (state() != State::Parsing)
	{
		auto& log = myLogger();
		log.errorStream() << "Can only analyze once, after parsing something.";
		return 1;
	}

	return d->structural_analysis();
}

int SteamWorks::PulleyScript::Parser::setup_sql()
{
	return d->setup_sql();
}

std::forward_list<SteamWorks::LDAP::BaseFilter> SteamWorks::PulleyScript::Parser::find_subscriptions()
{
	return d->find_subscriptions();
}

SteamWorks::PulleyScript::ExpectedActualCount SteamWorks::PulleyScript::Parser::find_backends(const char* libdir)
{
	return d->find_backends(libdir);
}

std::vector<varnum_t> SteamWorks::PulleyScript::Parser::Private::variables_for_generator(gennum_t g)
{
	bitset* b = gen_share_variables(m_prs.gentab, g);
	bitset_iter it;

	std::vector<varnum_t> names;
	names.reserve(bitset_count(b));

	unsigned int varcount = 0;

	bitset_iterator_init(&it, b);
	while (bitset_iterator_next_one(&it, NULL))
	{
		varnum_t v = bitset_iterator_bitnum(&it);
		if (var_get_kind(m_prs.vartab, v) != VARKIND_VARIABLE)
		{
			continue;
		}
		names.push_back(v);
		varcount++;
	}

	return names;
}

std::forward_list<SteamWorks::LDAP::BaseFilter> SteamWorks::PulleyScript::Parser::Private::find_subscriptions()
{
	auto& log = myLogger();
	log.debugStream() << "Finding parser subscriptions:";

	varnum_t world = var_find(m_prs.vartab, "world", VARKIND_VARIABLE);
	if (world == VARNUM_BAD)
	{
		log.warnStream() << "Script does not pull from world.";
		return {};
		// std::forward_list<std::string>();
	}

	m_variables_per_generator.clear();

	std::forward_list<LDAP::BaseFilter> filterexps;
	gennum_t count = gentab_count(m_prs.gentab);
	for (gennum_t i = 0; i < count; i++)
	{
		varnum_t v = gen_get_source(m_prs.gentab, i);
		LDAP::BaseFilter* filterexp = nullptr;

		if (v != world)
		{
			// Only look at expressions pulling from world
			log.warnStream() << "Expression" << (i + 1) << "pulls from" << var_get_name(m_prs.vartab, v)
					 << "ignored";
			continue;
		}
		else
		{
			filterexps.emplace_front();
			filterexp = &filterexps.front();
		}

		varnum_t b = gen_get_binding(m_prs.gentab, i);
		struct var_value* value = var_share_value(m_prs.vartab, b);

		auto bound_varnums = variables_for_generator(i);  // Variables on the right-hand side of binding
		m_variables_per_generator.emplace_back(bound_varnums.size());  // New vector of names

		unpack_binding(m_prs.vartab,
			       value->typed_blob.ptr,
			       value->typed_blob.len,
			       filterexp,
			       bound_varnums,
			       m_variables_per_generator.back());
	}

	for (gennum_t g = 0; g < count; g++)
	{
		log.debugStream() << "Variable names for generator " << g;
		for (const auto& f : m_variables_per_generator.at(g))
		{
			log.debugStream() << "    " << f.name;
		}
	}

	return filterexps;
}

struct DERItem
{
	der_t ptr;

	DERItem(struct squeal_blob* parm)
	    : ptr(nullptr)
	{
		uint8_t tag = 0x04;  // Universal tag octet string
		uint8_t len_len = 1;
		size_t len = parm->size;
		while (len >= 128)
		{
			len = len >> 8;
			len_len++;
		}

		ptr = (der_t)malloc(1 + len_len + parm->size);
		if (ptr)
		{
			ptr[0] = tag;
			if (len_len > 1)
			{
				ptr[1] = len_len | 0x80;
				len = parm->size;
				for (off_t i = len_len + 2; i >= 2; --i)
				{
					ptr[i] = len & 0xff;
					len = len >> 8;
				}
			}
			else
			{
				assert(parm->size < 128);
				ptr[1] = parm->size & 0x7f;
			}
			memcpy(ptr + 1 + len_len, parm->data, parm->size);
		}
	}

	DERItem()
	    : ptr(nullptr)
	{
	}

	~DERItem()
	{
		if (ptr)
		{
			free(ptr);
			ptr = nullptr;
		}
	}
};

struct DERArray
{
	int count;
	der_t* ptr;
	std::vector<DERItem> items;

	DERArray(int numactpart, struct squeal_blob* actparm)
	    : count(numactpart)
	    , ptr(numactpart > 0 ? (der_t*)calloc(numactpart, sizeof(der_t)) : nullptr)
	{
		if ((numactpart > 0) && (numactpart > items.capacity()))
		{
			items.reserve(numactpart);
		}

		if (ptr)
		{
			for (unsigned int i = 0; i < numactpart; i++)
			{
				items.emplace_back(&actparm[i]);
				ptr[i] = items[i].ptr;
			}
		}
		else
		{
			free(ptr);
			ptr = nullptr;
			count = 0;
		}
	}

	~DERArray()
	{
		free(ptr);
		ptr = nullptr;
		count = -1;
	}
};

static void ceebee(void* cbdata, int add_not_del, int actparmcount, struct squeal_blob* actparm)
{
	auto& log = myLogger();
	auto instance = reinterpret_cast<SteamWorks::PulleyBack::Instance*>(cbdata);
	log.debugStream() << "Callback called @" << cbdata << " name=" << instance->name()
			  << " valid=" << instance->is_valid() << " parameters=" << actparmcount;

	DERArray a(actparmcount, actparm);
	if (add_not_del == PULLEY_TUPLE_ADD)
	{
		log.debugStream() << "  .. adding";
		instance->add(a.ptr);
	}
	else if (add_not_del == PULLEY_TUPLE_DEL)
	{
		log.debugStream() << "  .. deleting";
		instance->del(a.ptr);
	}
	else
	{
		log.warnStream() << "Unknown callback command " << add_not_del << " ignored.";
	}
}

SteamWorks::PulleyScript::ExpectedActualCount
SteamWorks::PulleyScript::Parser::Private::find_backends(const char* libdir)
{
	auto& log = myLogger();

	int total = 0;
	int loaded = 0;

	m_backends.clear();
	drvnum_t count = drvtab_count(m_prs.drvtab);
	log.debugStream() << "Finding backend outputs (" << count << ')';
	for (drvnum_t drvidx = 0; drvidx < count; drvidx++)
	{
		log.debugStream() << "  .. getting driver #" << drvidx;
		const char* name = drv_get_module(m_prs.drvtab, drvidx);
		log.debugStream() << "  .. parameters for driver " << drvidx << ' ' << name;
		varnum_t binding = drv_get_module_parameters(m_prs.drvtab, drvidx);
		if (binding != VARNUM_BAD)
		{
			total++;
			struct var_value* value = var_share_value(m_prs.vartab, binding);
			std::vector<std::string> expressions;
			decode_parameter_binding(
			    m_prs.vartab, value->typed_blob.ptr, value->typed_blob.len, expressions);
			std::vector<std::string> reversed_expressions;
			reversed_expressions.resize(expressions.size());
			std::transform(expressions.cbegin(),
				       expressions.cend(),
				       reversed_expressions.rbegin(),
				       [](std::string s) { return s; });
			m_backends.emplace_front(name, reversed_expressions);

			varnum_t* var_list = nullptr;
			varnum_t var_count = 0;
			drv_share_output_variable_table(m_prs.drvtab, drvidx, &var_list, &var_count);

			const auto& b = m_backends.begin();
			b->driver = drvidx;
			b->varc = var_count;
			b->instance.reset(
			    new PulleyBack::Instance(PulleyBack::Loader(libdir, b->name).get_instance(*b)));
			if (b->instance->is_valid())
			{
				loaded++;
				squeal_configure_driver(m_sql.m_sql, drvidx, ceebee, b->instance.get());
			}
			else
			{
				log.warnStream() << "  .. loading backend #" << drvidx << " failed.";
				m_backends.pop_front();
			}
		}
	}

	return std::make_pair(total, loaded);
}

void SteamWorks::PulleyScript::Parser::remove_entry(const std::string& uuid)
{
	if (state() != State::Ready)
	{
		auto& log = myLogger();
		log.errorStream() << "Pulley setup was incomplete or failed (" << d->state_string() << "). "
				  << "Cannot remove entry.";
		return;
	}

	auto transaction = d->begin();
	d->remove_entry(uuid);
	transaction->m_dirty = true;
}

void SteamWorks::PulleyScript::Parser::add_entry(const std::string& uuid, const picojson::object& data)
{
	if (state() != State::Ready)
	{
		auto& log = myLogger();
		log.errorStream() << "Pulley setup was incomplete or failed (" << d->state_string() << "). "
				  << "Cannot add entry.";
		return;
	}

	auto transaction = d->begin();
	if (d->add_entry(uuid, data))
	{
		transaction->m_dirty = true;
	}
}

std::shared_ptr<SteamWorks::PulleyScript::BackendTransaction> SteamWorks::PulleyScript::Parser::begin()
{
	return d->begin();
}

void SteamWorks::PulleyScript::Parser::Private::remove_entry(const std::string& uuid)
{
	auto& log = myLogger();
	log.debugStream() << "Removing entry:" << uuid;

	gennum_t count = gentab_count(m_prs.gentab);
	for (gennum_t i = 0; i < count; i++)
	{
		hash_t h = gen_get_hash(m_prs.gentab, i);

		auto stream = log.debugStream();
		stream << "  .. generator " << i << " hash ";
		SteamWorks::Logging::log_hex(stream, (uint8_t*)&h, sizeof(h));

		squeal_delete_forks(m_sql.m_sql, i, uuid.c_str());
	}
}

/** @brief Gets the $dn (cached or computed) for the @p data
 *
 * If @p holder has a cached $dn value, returns that, otherwise find
 * the value, store it in the cache, and return it.
 *
 * Depending on where @p data comes from, it may have a $dn value
 * (e.g. stored by SteamWorks SyncRepl code as an additional value)
 * or it may need to be computed on-the-fly (e.g. when coming as
 * JSON or LDIF data).
 *
 */
static picojson::array& get_rdn_parts(std::optional<picojson::array>& holder, const picojson::object& data)
{
	// We've already got one
	if (holder.has_value())
	{
		return holder.value();
	}

	const auto it_dn = data.find("$dn");
	if (it_dn == data.cend())
	{
		// Need to calculate it from dn value
		const auto dn = SteamWorks::SplitString(',', data.at("dn").to_string()).detach();
		picojson::array rdn_parts(dn.size());
		size_t i = 0;
		for (const auto& dn_part : dn)
		{
			rdn_parts[i++] = picojson::value(dn_part);
		}
		holder = rdn_parts;
	}
	else
	{
		holder = it_dn->second.get<picojson::array>();
	}
	return holder.value();
}

bool SteamWorks::PulleyScript::Parser::Private::add_entry(const std::string& uuid, const picojson::object& data)
{
	auto& log = myLogger();
	log.debugStream() << "Adding entry:" << uuid;

	bool any_action = false;
	gennum_t count = gentab_count(m_prs.gentab);
	// Size the blobs vector only once
	int max_blobs_count = 0;
	for (gennum_t i = 0; i < count; i++)
	{
		auto c = variable_names(i).size();
		if (c > max_blobs_count)
		{
			max_blobs_count = c;
		}
	}
	if (max_blobs_count < 1)
	{
		return false;
	}
	std::vector<struct squeal_blob> blobs(max_blobs_count);

	for (gennum_t i = 0; i < count; i++)
	{
		hash_t h = gen_get_hash(m_prs.gentab, i);

		// Block to scope `stream`
		{
			auto stream = log.debugStream();
			stream << "  .. generator " << i << " hash ";
			SteamWorks::Logging::log_hex(stream, (uint8_t*)&h, sizeof(h));
		}

		// Build the list of variable names (because the info contains
		// more than just the name, variable_names() is a bit misleading).
		const auto& variable_info = variable_names(i);
		std::vector<std::string> attribute_names;
		attribute_names.reserve(variable_info.size());
		bool valid_match = true;
		for (const auto& f : variable_info)
		{
			attribute_names.emplace_back(f.name);
			if (f.subject == BNDO_SUBJ_ATTR)
			{
				bool found = true;
				std::string name = attributeName(data, f.name, found);
				if (found)
				{
					log.debugStream() << "  .. generate with attribute " << name;
				}
				else
				{
					log.debugStream() << "  .. desired attribute " << f.name << " is missing.";
					valid_match = false;
					break;
				}
			}
		}
		if (!valid_match)
		{
			continue;
		}

		MultiIterator it(data, attribute_names);
		// Now binding the non-ATTR variables, which need the RDN components
		// of the object (for bindings of type RDN).
		std::optional<picojson::array> hold_rdn_parts;  // get it only if there is a binding
		unsigned int attr_index = 0;
		unsigned int rdn_index = 0;
		for (const auto& f : variable_info)
		{
			auto rdn_parts = get_rdn_parts(hold_rdn_parts, data);
			if (f.subject == BNDO_SUBJ_RDN)
			{
				log.debugStream() << "Binding RDN " << f.name;
				const int name_length = f.name.length();
				bool rdn_found = false;
				// Look at RDN parts until we find this one
				for (; rdn_index < rdn_parts.size(); ++rdn_index)
				{
					log.debugStream() << ".. comparing " << rdn_parts[rdn_index];
					std::string rdn_part = rdn_parts[rdn_index].to_str();
					if (rdn_part.length() > name_length && rdn_part[name_length] == '='
					    && strncasecmp(f.name.data(), rdn_part.data(), name_length) == 0)
					{
						log.debugStream() << ".. matched " << rdn_part;
						it.setConstant(attr_index, rdn_part.substr(name_length + 1));
						rdn_found = true;
						break;
					}
				}
				if (!rdn_found)
				{
					log.debugStream() << "  .. desired RDN component " << f.name << " is missing.";
					valid_match = false;
					break;
				}
			}
			attr_index++;
		}
		if (!valid_match)
		{
			continue;
		}

		while (!it.is_done())
		{
			MultiIterator::value_t v = it.next();
			unsigned int blobnum = 0;
			for (const auto& f : v)
			{
				blobs[blobnum].data = (void*)f.c_str();
				blobs[blobnum].size = f.length();
				blobnum++;
			}

			any_action = true;
			squeal_insert_fork(m_sql.m_sql, i, uuid.c_str(), variable_names(i).size(), blobs.data());
		}
	}

	return any_action;
}

void SteamWorks::PulleyScript::Parser::Private::commit()
{
	auto& log = myLogger();
	for (const auto& backend : m_backends)
	{
		int prep = backend.instance->prepare();
		log.debugStream() << "  .. backend " << backend.name << " prepare " << prep;
		if (prep == 0)
		{
			// Failure, clear up everything.
			goto fail;
		}
	}

	for (const auto& backend : m_backends)
	{
		int comm = backend.instance->commit();
		log.debugStream() << "  .. backend " << backend.name << " commit " << comm;
		if (comm == 0)
		{
			goto fail;
		}
	}

	return;

fail:
	for (const auto& backend : m_backends)
	{
		backend.instance->rollback();
	}
}


SteamWorks::PulleyScript::BackendParameters::BackendParameters(std::string n,
							       const std::vector<std::string>& expressions)
    : name(n)
    , varc(0)
    , argc(expressions.size())
    , argv(nullptr)
{
	if (argc > 0)
	{
		argv = (char**)calloc(argc, sizeof(char*));
	}
	if (argv)
	{
		unsigned int i = 0;
		for (const auto& s : expressions)
		{
			argv[i++] = strdup(s.c_str());
		}
	}
}

SteamWorks::PulleyScript::BackendParameters::~BackendParameters()
{
	if (argv)
	{
		for (unsigned int i = 0; i < argc; i++)
		{
			free(argv[i]);
		}
		free(argv);
		argv = nullptr;
	}
}

#ifndef NDEBUG
std::ostringstream& SteamWorks::PulleyScript::operator<<(std::ostringstream& s,
							 const SteamWorks::PulleyScript::BackendParameters& b)
{
	s << b.name << '(';
	for (unsigned int i = 0; i < b.argc; i++)
	{
		s << b.argv[i];
		if (i < (b.argc - 1))
		{
			s << ", ";
		}
	}
	s << ')';
	return s;
}
#endif
