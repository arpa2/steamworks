/*
 * SPDX-FileCopyrightText: 2016 InternetWide.org and the ARPA2.net project
 * SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 * SPDX-License-Identifier: BSD-2-Clause
 */

/**
 * This is a C++ wrapper around the PulleyScript parser / generator / etc.
 * complex. It handles the bindings "machine language".
 */
#ifndef STEAMWORKS_PULLEYSCRIPT_BINDINGPP_H
#define STEAMWORKS_PULLEYSCRIPT_BINDINGPP_H

#include "binding.h"
#include "variable.h"

#include "swldap/filter.h"

#include <string>
#include <vector>

namespace SteamWorks
{

namespace PulleyScript
{

struct Variable
{
	std::string name;
	varnum_t number = 0;
	int subject = BNDO_SUBJ_NONE;
};

/**
 * Unpack the binding into C++ structures. As a side effect, explain it on stdout.
 *
 * Explain the coded @p binding (for machine-language explanation,
 * see binding.h) relative to the variable table @p vars.
 * Outputs to the logfile.
 *
 * If @p filterexp is not a null-pointer, builds an LDAP filter
 * expression for this binding based on the constant-comparisons
 * found in the binding.
 *
 * The vector @p bound_varnums lists the bound variables for
 * this generator (e.g. the variables associated with the generator
 * being explained), and the vector @p variable_names (which is
 * assumed to be the same size) is filled with the attributes
 * bound to those variables.
 */
void unpack_binding(vartab* vars,
		    uint8_t* binding,
		    uint32_t len,
		    LDAP::BaseFilter* filterexp,
		    const std::vector<varnum_t>& bound_varnums,
		    std::vector<Variable>& variable_names);

/**
 * Decode the coded @p binding (for machine-language explanation,
 * see binding.h) relative to the variable table @p vars. This is
 * assumed to be a binding program for backend-parameters (e.g. only
 * CMP statements).
 *
 * As a side-effect, adds strings to @p expressions for each
 * binding, in the original syntax.
 */
void decode_parameter_binding(vartab* vars, uint8_t* binding, uint32_t len, std::vector<std::string>& expressions);

}  // namespace PulleyScript
}  // namespace SteamWorks

#endif
