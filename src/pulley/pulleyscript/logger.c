/*
 *   SPDX-FileCopyrightText: 2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

/**
 * This file is for use with C programs that call into write_logger()
 * but don't actually link with swcommon (which is the bridge from
 * write_logger() to log4cpp). So instead of logging, this just goes
 * back to printf().
 */

#include <stdio.h>

void write_logger(const char* logname, const char* message)
{
	printf("%s: DEBUG: %s\n", logname, message);
}

void warning_logger(const char* logname, const char* message)
{
	fprintf(stderr, "%s: WARNING: %s\n", logname, message);
}

void log_as_debug(const char* pluginname, const char* message)
{
	printf("%s: DEBUG: %s\n", pluginname, message);
}

void log_as_warning(const char* pluginname, const char* message)
{
	fprintf(stderr, "%s: WARNING: %s\n", pluginname, message);
}
