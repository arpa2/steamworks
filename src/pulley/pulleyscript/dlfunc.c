/*
 *   SPDX-FileCopyrightText: 2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#ifndef HAVE_DLFUNC

#include "dlfunc.h"
#include <dlfcn.h>

/* Answer from StackOverflow.com, 1144107. */
/* copied from FreeBSD, lib/libc/gen/dlfunc.c */

dlfunc_t dlfunc(const void* handle, const void* symbol)
{
	union
	{
		void* d;
		dlfunc_t f;
	} rv;
	rv.d = dlsym((void*)handle, (void*)symbol); /* Conversion warning */
	return rv.f;
}
#endif
