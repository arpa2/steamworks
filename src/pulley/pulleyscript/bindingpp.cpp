/*
 *   SPDX-FileCopyrightText: 2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#include "bindingpp.h"
#include "binding.h"

#include "common/logger.h"

#include <assert.h>

static SteamWorks::Logging::Logger& myLogger()
{
	return SteamWorks::Logging::getLogger("steamworks.pulley.script");
}

static inline varnum_t extract_varnum(uint8_t* p)
{
	return *(varnum_t*)p;
}

void SteamWorks::PulleyScript::decode_parameter_binding(vartab* vars,
							uint8_t* binding,
							uint32_t len,
							std::vector<std::string>& expressions)
{
	auto& log = myLogger();

	{
		auto d = log.debugStream();
		d << "Decode binding @" << (void*)binding << ' ';
		SteamWorks::Logging::log_hex(d, binding, len);
	}

	unsigned int indent = 0;
	uint8_t* p = binding;
	uint8_t* end = p + len;
	// Count number of compared-vars; if there is more than one, create
	// a conjunction-expression for the LDAP search-expression.
	unsigned int varcount = 0;

	while (p < end)
	{
		auto d = log.debugStream();
		SteamWorks::Logging::log_indent(d, indent);

		uint8_t opcode = (*p) & 0xf;
		uint8_t operand_t = (*p) & 0x30;

		varnum_t v0 = VARNUM_BAD, v1 = VARNUM_BAD;

		std::string s;
		switch (opcode)
		{
		case BNDO_ACT_CMP:
			v0 = extract_varnum(p + 1);
			v1 = extract_varnum(p + 1 + sizeof(varnum_t));

			s = var_get_name(vars, v0);
			s.append(1, '=');
			s.append(var_get_name(vars, v1));
			expressions.push_back(s);
			p += 1 + 2 * sizeof(varnum_t);
			break;
		case BNDO_ACT_DONE:
			goto done;
		default:
			d << "BAD OPCODE ";
			SteamWorks::Logging::log_hex(d, &opcode, 1);
			goto done;
		}
	}
done:;
}

void SteamWorks::PulleyScript::unpack_binding(vartab* vars,
					      uint8_t* binding,
					      uint32_t len,
					      LDAP::BaseFilter* filterexp,
					      const std::vector<varnum_t>& bound_varnums,
					      std::vector<SteamWorks::PulleyScript::Variable>& variable_names)
{
	static const char None[] = "None";
	auto uNone = [](const char* s) -> const char* { return s ? s : None; };

	auto& log = myLogger();

	{
		auto d = log.debugStream();
		d << "Explain binding @" << (void*)binding << ' ';
		SteamWorks::Logging::log_hex(d, binding, len);
	}

	unsigned int indent = 0;
	uint8_t* p = binding;
	uint8_t* end = p + len;
	// Count number of compared-vars; if there is more than one, create
	// a conjunction-expression for the LDAP search-expression.
	unsigned int varcount = 0;

	bool building_base = true;

	while (p < end)
	{
		auto d = log.debugStream();
		SteamWorks::Logging::log_indent(d, indent);

		uint8_t opcode = (*p) & 0xf;
		uint8_t operand_t = (*p) & 0x30;

		const char* operand_s = nullptr;
		switch (operand_t)
		{
		case BNDO_SUBJ_NONE:
			operand_s = nullptr;
			break;
		case BNDO_SUBJ_ATTR:
			operand_s = "attr";
			break;
		case BNDO_SUBJ_RDN:
			operand_s = "RDN";
			break;
		case BNDO_SUBJ_DN:
			operand_s = "DN";
			break;
		}

		varnum_t v0 = VARNUM_BAD, v1 = VARNUM_BAD;
		switch (opcode)
		{
		case BNDO_ACT_DOWN:
			d << "DOWN";
			if (operand_s)
			{
				d << ' ' << operand_s;
			}
			indent++;
			p++;
			break;
		case BNDO_ACT_HAVE:
			building_base = false;
			d << "HAVE " << uNone(operand_s) << ' ' << var_get_name(vars, extract_varnum(p + 1));
			p += 1 + sizeof(varnum_t);
			break;
		case BNDO_ACT_BIND:
			building_base = false;
			v0 = extract_varnum(p + 1);
			v1 = extract_varnum(p + 1 + sizeof(varnum_t));
			{
				const char* v0_name = (v0 == VARNUM_BAD) ? "(null)" : var_get_name(vars, v0);
				const char* v1_name = (v1 == VARNUM_BAD) ? "(null)" : var_get_name(vars, v1);
				d << "BIND " << uNone(operand_s) << ' ' << v0_name << " (" << v0 << ") ~ " << v1_name
				  << " (" << v1 << ')';
				for (unsigned int i = 0; i < bound_varnums.size(); i++)
				{
					if (v1 == bound_varnums[i])
					{
						std::string s;
						if ((operand_t == BNDO_SUBJ_DN) && (v0 == VARNUM_BAD))
						{
							s = std::string("dn");
						}
						else
						{
							s = v0_name;
						}
						variable_names[i] = { s, v1, operand_t };
					}
				}
			}
			p += 1 + 2 * sizeof(varnum_t);
			break;
		case BNDO_ACT_CMP:
			v0 = extract_varnum(p + 1);
			v1 = extract_varnum(p + 1 + sizeof(varnum_t));
			d << "CMP  " << uNone(operand_s) << ' ' << var_get_name(vars, v0) << '='
			  << var_get_name(vars, v1);
			if (filterexp && (var_get_kind(vars, v1) == VARKIND_CONSTANT))
			{
				// For constants, the name is the value. Due to syntactical oddities
				// in the pulleyscript, we can have a constant value that is
				//  - numeric
				//  - quoted with double-quotes
				// in the LDAP filter-expression, though, we need to drop those quotes.
				const char* cval = var_get_name(vars, v1);
				if (building_base)
				{
					auto& base = filterexp->base();
					if (!base.empty())
					{
						base.insert(0, 1, ',');
					}
					std::string basecmp = var_get_name(vars, v0);
					basecmp.append(1, '=');
					basecmp.append(cval);
					base.insert(0, basecmp);
				}
				else
				{
					auto& filter = filterexp->filter();
					filter.append(1, '(');
					filter.append(var_get_name(vars, v0));
					filter.append(1, '=');
					if (cval[0] == '"')
					{
						// Avoid the start and end "
						size_t len = strlen(cval);
						assert(len >= 2);
						filter.append(cval + 1, len - 2);
					}
					else
					{
						filter.append(cval);
					}
					filter.append(1, ')');
					varcount++;
				}
			}
			p += 1 + 2 * sizeof(varnum_t);
			break;
		case BNDO_ACT_OBJECT:
			building_base = false;
			d << "OBJECT";
			p += 1;
			break;
		case BNDO_ACT_DONE:
			d << "DONE";
			goto done;
		default:
			d << "UNKNOWN ";
			SteamWorks::Logging::log_hex(d, &opcode, 1);
			goto done;
		}
	}
done:
	if (filterexp && (varcount > 1))
	{
		// Turn (v0=c0)(v1=c1)...(vn=cn) into a proper conjunction
		// according to LDAP filter syntax.
		filterexp->filter().insert(0, "(&");
		filterexp->filter().append(1, ')');
	}
	log.debugStream() << " ..done binding @" << (void*)binding;
}
