/*
 * This is SteamWorks, a distribution and transformation system for LDAP data
 *
 * The pulleyback test application `pulleyscript` can load Pulley plugins,
 * run Pulley scripts standalone, and in general is the tool for testing and
 * debugging Pulley parts before deploying them.
 */

/*
 *   SPDX-License-Identifier: BSD-2-Clause
 *   SPDX-FileCopyrightText: Copyright (c) 2014, 2015 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: Copyright (c) 2017, Adriaan de Groot <groot@kde.org>
 *   SPDX-FileCopyrightText: Copyright 2019,2020 Adriaan de Groot <groot@kde.org>
 */

#include "pulley.h"

#include "pulleyscript/backend.h"
#include "pulleyscript/parserpp.h"

#include "common/fcgi.h"
#include "common/logger.h"

#include <fstream>
#include <string>

#include <getopt.h>
#include <stdio.h>
#include <unistd.h>

extern const char* squeal_use_dbdir;

static const char progname[] = "PulleyScript Test";
static const char version[] = "v0.2";
static const char copyright[] = "Copyright (C) 2014-2021 InternetWide.org and the ARPA2.net project";

static void help()
{
	fputs("Usage: pulleyscript [options...] [arguments...]\n\n"

	      /* Loading a plugin with args */
	      "To load a plugin to check API and symbols (loads plugins):\n"
	      "\tpulleyscript -p [options] <plugin> [plugin-args...]\n"
	      "Optional [plugin-args] are passed to pulleyback_open() in the plugin.\n"
	      "\n"

	      /* Syntax-checking a script */
	      "To load a script to check syntax (writes SQL):\n"
	      "\tpulleyscript -s [options] <script.ply>\n"
	      "\n"

	      /* Simulate running a script */
	      "To simulate running a script with JSON changes (writes SQL, loads plugins):\n"
	      "\tpulleyscript -s [options] <script.ply> <change.json> ..\n"
	      "This is the same as syntax-checking, but with file-arguments after the script.\n"
	      "\n"

	      /* Running a script */
	      "To load and run a Pulley script (writes SQL, load plugins):\n"
	      "\tpulleyscript -S -c <uri> -b <base> [options] <script.ply>\n"
	      "These options must be given to run a script:\n"
	      " -c <uri>   \tto specify the LDAP server (ldap, ldaps or ldapi URI)\n"
	      " -b <base>  \tto specify the base binding for syncrepl\n"
	      "This option may be given when running a script:\n"
	      " -N         \tdo not run syncrepl loop (only poll once)\n"
	      "\n"

	      /* General-purpose arguments */
	      "Options which may be specified any time:\n"
	      " -L <libdir>\tto specify where backend plugins are loaded (modes that load plugins).\n"
	      " -D <dbdir> \tto specify where to store intermediate SQL (modes that write SQL)\n"
	      " -v         \tbe verbose (all modes)\n"
	      " --logging <properties>\tconfigure logging explicitly\n"
	      "\n"
	      "Either -p (plugin) or -s (script) or -S (run-script) mode must be indicated.\n\n",
	      stderr);
}

static void usage()
{
	fputs("Usage: pulleyscript [options...] [arguments...]\n\n"
	      "\tpulleyscript -p [options] <plugin> [plugin-args...]\n"
	      "\tpulleyscript -s [options] <script.ply>\n"
	      "\tpulleyscript -s [options] <script.ply> <change.json> ..\n"
	      "\tpulleyscript -S -c <uri> -b <base> [options] <script.ply>\n"
	      "\tpulleyscript --help\n",
	      stderr);
}

struct Settings
{
	const char* lib_dir = nullptr;
	const char* connect_uri = nullptr;
	const char* base = nullptr;
	const char* properties = nullptr;  ///< filename for logging configuration
	bool do_loop = true;
	bool verbose = false;
	// -D <dbdir> is stored in an external Squeal variable

	/** @brief Configure and return the logger for this application
	 *
	 * Logging settings depend on the value of `verbose` the **first**
	 * time this function is called. Do not call it before `verbose`
	 * has reached its "final" value.
	 */
	SteamWorks::Logging::Logger& logger() const
	{
		static bool ready = false;
		static SteamWorks::Logging::Manager logManager(properties ? properties : "pulleyscript.properties",
							       verbose ? SteamWorks::Logging::DEBUG
								       : SteamWorks::Logging::INFO);
		if (!ready)
		{
			SteamWorks::Logging::getRoot().debugStream() << "SteamWorks " << progname << ' ' << copyright;
			ready = true;
		}

		return SteamWorks::Logging::getLogger("steamworks.pulley.application.test");
	}
};

static int do_backend(const Settings& settings, int remaining_argc, char** remaining_argv)
{
	const char* backend_name = remaining_argv[0];
	settings.logger().debugStream() << "Loading plugin '" << backend_name << '\'';
	SteamWorks::PulleyBack::Loader loader(settings.lib_dir, backend_name);
	// Passes in special value -1 for varc so that the backend knows it is being tested.
	// No backend-plugin-name is passed in; the plugin knows what it is.
	SteamWorks::PulleyBack::Instance backend = loader.get_instance(remaining_argc - 1, remaining_argv + 1, -1);
	if (!backend.is_valid())
	{
		settings.logger().errorStream() << "Could not load plugin '" << backend_name << '\'';
		return 1;
	}
	return 0;
}

/** @brief Check the syntax of the given @p script_file
 *
 * Returns 0 on success, non-zero on failure.
 */
static int do_syntax(SteamWorks::PulleyScript::Parser& p, SteamWorks::Logging::Logger& logger, const char* script_file)
{
	if (p.read_file(script_file))
	{
		logger.errorStream() << "Could not read script " << script_file;
		return 1;
	}
	else
	{
		logger.debugStream() << "Done reading script " << script_file;
		if (p.structural_analysis())
		{
			logger.errorStream() << "Parser structural analysis returned:" << p.state_string();
			return 1;
		}
		if (p.setup_sql())
		{
			logger.errorStream() << "Parser SQL setup returned:" << p.state_string();
			return 1;
		}
	}
	// If broken, return failure (1); success returns 0
	return p.state() == SteamWorks::PulleyScript::Parser::State::Broken ? 1 : 0;
}

/** @brief Simulate a transaction triggered by JSON data @p v
 *
 * Returns whether there was a failure: @c true means "something is wrong"
 * and @c false means that the transaction was successful.
 */
static bool
simulate_transaction(SteamWorks::Logging::Logger& log, SteamWorks::PulleyScript::Parser& prs, const picojson::object& v)
{
	constexpr const bool Failure = true;
	constexpr const bool Success = false;

	if (v.count("values") == 0)
	{
		log.errorStream() << "No 'values' element in JSON.";
		return Failure;
	}

	if (!v.at("values").is<picojson::array>())
	{
		log.errorStream() << "Element 'values' is not an array.";
		return Failure;
	}

	bool add_not_delete;
	{
		std::string verb;
		if (v.count("verb") > 0)
		{
			const auto& verb_val = v.at("verb");
			if (verb_val.is<std::string>())
			{
				verb = verb_val.to_str();
			}
		}
		if (verb == "add")
		{
			add_not_delete = true;
		}
		else if (verb == "del")
		{
			add_not_delete = false;
		}
		else
		{
			log.errorStream() << "Verb '" << verb << "' is not recognized (use add or del).";
			return Failure;
		}
	}

	auto transaction = prs.begin();

	const auto& values = v.at("values").get<picojson::array>();
	unsigned int index = 0;
	unsigned int count = 0;
	for (const auto& v : values)
	{
		index++;
		if (!v.is<picojson::object>())
		{
			log.warnStream() << "values[" << index - 1 << "] is not an object.";
			continue;
		}

		auto uuid_v = v.get("uuid");
		std::string uuid = uuid_v.is<picojson::null>() ? std::string() : uuid_v.to_str();
		if (uuid.empty())
		{
			log.warnStream() << "values[" << index - 1 << "] has no UUID.";
			continue;
		}

		if (add_not_delete)
		{
			prs.add_entry(uuid, v.get<picojson::object>());
		}
		else
		{
			prs.remove_entry(uuid);
		}
		count++;
	}
	log.debugStream() << "Processed " << count << " values.";
	return Success;
}


static int do_simulate(const Settings& settings, int remaining_argc, char** remaining_argv)
{
	const char* script_file = remaining_argv[0];
	if (settings.connect_uri || settings.base)
	{
		if (remaining_argc <= 1)
		{
			fputs("Warning: no need for -c or -b when checking script for syntax.\n", stderr);
		}
		else
		{
			fputs("Warning: no need for -c or -b when simulating script run.\n", stderr);
		}
	}
	SteamWorks::PulleyScript::Parser p;
	if (do_syntax(p, settings.logger(), script_file) != 0)
	{
		return 1;
	}

	if (remaining_argc <= 1)
	{
		// No simulation, so success
		return 0;
	}
	p.find_subscriptions();
	p.find_backends(settings.lib_dir);
	if (p.state() == SteamWorks::PulleyScript::Parser::State::Broken)
	{
		return 1;
	}

	settings.logger().debugStream() << "Simulating " << (remaining_argc - 1) << " transactions from JSON files.";

	picojson::value v;
	bool any_failures = false;
	for (int file_index = 1; file_index < remaining_argc; ++file_index)
	{
		const char* json_filename = remaining_argv[file_index];
		std::ifstream input(json_filename);
		picojson::parse(v, input);

		if (v.is<picojson::object>())
		{
			settings.logger().debugStream() << "JSON object read from " << json_filename;
			any_failures |= simulate_transaction(settings.logger(), p, v.get<picojson::object>());
		}
		else
		{
			settings.logger().errorStream() << "Could not read JSON from " << json_filename;
			any_failures = true;
		}
	}

	return any_failures ? 1 : 0;
}

static int do_run(const Settings& settings, int remaining_argc, char** remaining_argv)
{
	if (!settings.connect_uri || !settings.base)
	{
		fputs("Error: both -c and -b must be set to run a script.\n", stderr);
		return 1;
	}
	if (remaining_argc > 1)
	{
		fputs("Warning: additional arguments ignored when running a script.\n", stderr);
	}

	const char* script_file = remaining_argv[0];

	settings.logger().debugStream() << "Running Pulley standalone with script" << script_file;
	PulleyDispatcher* dispatcher = new PulleyDispatcher(settings.lib_dir);
	if (dispatcher->do_connect(settings.connect_uri))
	{
		settings.logger().errorStream() << "Could not connect to " << settings.connect_uri;
		return 1;
	}
	if (dispatcher->do_script(script_file))
	{
		settings.logger().errorStream() << "Could not load script " << script_file;
		return 1;
	}
	if (dispatcher->do_autofollow(settings.base))
	{
		settings.logger().errorStream() << "Could not load all plugins for base " << settings.base;
		return 1;
	}
	settings.logger().debugStream() << "Starting main loop ..";
	do
	{
		dispatcher->poll();
		sleep(1);
	} while (settings.do_loop);
	settings.logger().debugStream() << "Ended main loop.";
	return 0;
}


int main(int argc, char** argv)
{
	if (argc < 2)
	{
		usage();
		return 1;
	}

	int initial_argument = 0;
	Settings settings;  // NOTE: do not call logger() before settings.verbose is set

	enum class RunMode
	{
		None,
		Plugin,
		ScriptSyntax,
		ScriptRun
	};
	RunMode mode = RunMode::None;

	static const char mode_warning[]
	    = "Warning: only one of -p (plugin) -s (script) and -S (run-script) can be used.\n";

	int special_longopt = 0;
	enum longopt
	{
		logging = 1,
	};

	const struct option longopts[] = {
		{ "script-syntax", no_argument, 0, 's' },
		{ "script-run", no_argument, 0, 'S' },
		{ "plugin", no_argument, 0, 'p' },
		{ "base", required_argument, 0, 'b' },
		{ "connect", required_argument, 0, 'c' },
		{ "no-loop", no_argument, 0, 'N' },
		{ "sqldir", required_argument, 0, 'D' },
		{ "libdir", required_argument, 0, 'L' },
		{ "verbose", no_argument, 0, 'v' },
		{ "logging", required_argument, &special_longopt, longopt::logging },
		{ "version", no_argument, 0, 'V' },
		{ "help", no_argument, 0, 'h' },
		{ 0, 0, 0, 0 },
	};

	int opt;
	while ((opt = getopt_long(argc,
				  argv,
				  "+"  // end at first non-option
				  "sSp"  // mode
				  "b:c:"  // LDAP settings
				  "N"  // run-mode flag
				  "D:L:"  // directory-settings
				  "vVh?",  // meta
				  longopts,
				  nullptr))
	       != -1)
	{
		switch (opt)
		{
		case 's':
			if (mode != RunMode::None)
			{
				fputs(mode_warning, stderr);
			}
			mode = RunMode::ScriptSyntax;
			break;
		case 'S':
			if (mode != RunMode::None)
			{
				fputs(mode_warning, stderr);
			}
			mode = RunMode::ScriptRun;
			break;
		case 'p':
			if (mode != RunMode::None)
			{
				fputs(mode_warning, stderr);
			}
			mode = RunMode::Plugin;
			break;
		case 'c':
			settings.connect_uri = optarg;
			break;
		case 'L':
			settings.lib_dir = optarg;
			break;
		case 'D':
			squeal_use_dbdir = optarg;
			break;
		case 'b':
			settings.base = optarg;
			break;
		case 'N':
			settings.do_loop = false;
			break;
		case 'v':
			settings.verbose = true;
			break;
		case 'V':
			fprintf(stdout, "%s %s\n%s\n", progname, version, copyright);
			return 0;
		case 0:
			// Here, the **real** option code is stored (it's a long-only
			// option) in special_longopt; all long-only options return 0
			switch (special_longopt)
			{
			case longopt::logging:
				settings.verbose = true;
				settings.properties = optarg;
				break;
			default:
				usage();
				return 1;
			}
			special_longopt = 0;
			break;
		case 'h':
		case '?':
			help();
			return 0;
		default:
			usage();
			return 1;
		}
	}

	initial_argument = optind;
	if (initial_argument < 2 || argc <= initial_argument)
	{
		// No argument given; all modes require at least one argument
		usage();
		return 2;
	}

	switch (mode)
	{
	case RunMode::None:
		usage();
		return 2;
	case RunMode::Plugin:
		return do_backend(settings, argc - initial_argument, argv + initial_argument);
	case RunMode::ScriptSyntax:
		return do_simulate(settings, argc - initial_argument, argv + initial_argument);
	case RunMode::ScriptRun:
		return do_run(settings, argc - initial_argument, argv + initial_argument);
	}

	return 0;
}
