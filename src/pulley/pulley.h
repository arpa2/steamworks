/*
 *   SPDX-FileCopyrightText: 2014-2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#ifndef STEAMWORKS_PULLEY_H
#define STEAMWORKS_PULLEY_H

#include "common/verb.h"

#include <memory>

class PulleyDispatcher : public VerbDispatcher
{
private:
	class Private;
	std::unique_ptr<Private> d;

	typedef enum
	{
		disconnected = 0,
		connected,
		stopped
	} State;
	State m_state;

public:
	/** @brief Create a Pulley
	 *
	 * The pulley is configured later via JSON commands to load scripts,
	 * connect to LDAP servers, and load backend plugins. For debugging purposes,
	 * @p libdir can be set to a non-nullptr value (which should live as
	 * long as the dispatcher; no ownership is taken). Plugins will be
	 * loaded from the given @p libdir, or, if it is @c nullptr (the default),
	 * from the standard plugin directory.
	 */
	PulleyDispatcher(const char* libdir = nullptr);

	int exec(const std::string& verb, const Values& values, Object& response) override;
	void poll() override;

	State state() const { return m_state; }

	/** Load a PulleyScript script. This overload with a const char *
	 *  parameter loads a file from the local filesystem, for use
	 *  before the Pulley connects to the LDAP server.
	 */
	int do_script(const char* filename);

	/** Connect to an LDAP server at the given URI. This can be
	 *  used to programmatically connect, rather than interpreting
	 *  a command via exec();
	 */
	int do_connect(const std::string& uri);

	/** Add syncrepl to the given @p base and @p filter. This
	 * can be used to programmatically follow a part of the DIT,
	 * rather than interpreting a command via exec().
	 */
	int do_follow(const std::string& base, const std::string& filter);

	int do_autofollow(const std::string& base);

protected:
	/** Connect to the upstream (e.g. source) LDAP server.
	 *  This is where the pulley gets its information. */
	int do_connect(const Values& values, Object& response);
	int do_stop(const Values& values);
	int do_serverinfo(const Values& values, Object& response);

	/** Start following a (subtree-) DIT. This starts up SyncRepl
	 *  for that DIT. */
	int do_follow(const Values& values, Object& response);
	/** Stop following a previously followed DIT. This terminates
	 *  SyncRepl for that DIT. */
	int do_unfollow(const Values& values, Object& response);

	/** Debugging method, dump the tree of stored DIT entries. */
	int do_dump_dit(const Values& values, Object& response);

	/** Drop all stored state, and restart LDAP SyncRepl. */
	int do_resync(const Values& values, Object& response);

	/** Load a PulleyScript script. */
	int do_script(const Values& values, Object& response);

private:
	/// @brief Implementation of follow. Only call this when connected.
	int do_follow(const std::string& base, const std::string& filter, Object& response);
};


#endif
