/*
 *   SPDX-FileCopyrightText: 2014-2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#include "pulley.h"

#include "pulleyscript/parserpp.h"

#include "common/fcgi.h"
#include "common/logger.h"

#include <getopt.h>

static const char progname[] = "Pulley";
static const char version[] = "v0.1";
static const char copyright[] = "Copyright (C) 2014-2016 InternetWide.org and the ARPA2.net project";

static void version_info()
{
	printf("SteamWorks %s %s\n", progname, version);
	printf("%s\n", copyright);
}

static void version_usage()
{
	printf(R"(
Usage:
    pulley scriptfile [...]


)");
}

static void version_help()
{
	version_info();
	printf(R"(
Use the Pulley to output configuration received from other
SteamWorks components to a local configuration though the
backends available on the system.

Backend plug-ins will be loaded from the standard plugin path.
The PulleyScript scripts are read once, at start-up.

)");
	version_usage();
}


int main(int argc, char** argv)
{
	SteamWorks::Logging::Manager logManager("pulley.properties");
	SteamWorks::Logging::getRoot().debugStream() << "SteamWorks Pulley" << copyright;

	const struct option longopts[] = {
		{ "version", no_argument, 0, 'v' },
		{ "help", no_argument, 0, 'h' },
		{ 0, 0, 0, 0 },
	};


	bool carry_on = true;
	int index;
	int iarg = 0;

	while (iarg != -1)
	{
		iarg = getopt_long(argc, argv, "vh", longopts, &index);

		switch (iarg)
		{
		case 'v':
			version_info();
			carry_on = false;
			break;
		case 'h':
			version_help();
			carry_on = false;
			break;
		case '?':
			carry_on = false;
			break;
		case -1:
			// End of options, detected next time around
			break;
		default:
			abort();
		}
	}
	if (!carry_on)
	{
		// Either --help or --version or an option error
		return 1;
	}


	PulleyDispatcher* dispatcher = new PulleyDispatcher();

	while (optind < argc)
	{
		int prsret = dispatcher->do_script(argv[optind++]);
		if (prsret)
		{
			//Error in parsing file
			return 1;
		}
	}

	SteamWorks::FCGI::init_logging("steamworks.pulley.fcgi");
	SteamWorks::FCGI::mainloop(dispatcher);

	return 0;
}
