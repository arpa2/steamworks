#! /bin/sh
#
# SPDX-FileCopyrightText: 2021 Adriaan de Groot <groot@kde.org>
# SPDX-License-Identifier: BSD-2-Clause
#
# This is an example of a sequence of HTTP requests that can be sent
# to the Crank demonstrative CRUD functionality. It assumes that
# the Crank is running on the local host (see section *Testing the Crank*
# in the README).

# Set a default port 
test -n "$CRANK_PORT" || CRANK_PORT="8000"
COMMAND="cgi-fcgi -bind -connect localhost:$CRANK_PORT"

# Where the data lives in the DIT
BASE="o=arpa2.net,ou=InternetWide"

# Three people, AA AB and AC
PERSON_AA="cn=Anna Anny,ou=People,$BASE"
PERSON_AA_DATA="cn=Anna Anny,description=Event Organizer,objectClass=person,sn=Anny"
PERSON_AB="cn=Alice Blick,ou=People,$BASE"
PERSON_AB_DATA="cn=Alice Blick,description=CEO,objectClass=person,sn=Blick"
PERSON_AC="cn=Astrid Casals,ou=People,$BASE"
PERSON_AC_DATA="cn=Astrid Casals,description=Developer Advocate,objectClass=person,sn=Casals"


# Add all three people
REQUEST_METHOD=PUT \
PATH_INFO="$PERSON_AA" \
QUERY_STRING="$PERSON_AA_DATA???" \
$COMMAND

REQUEST_METHOD=PUT \
PATH_INFO="$PERSON_AB" \
QUERY_STRING="$PERSON_AB_DATA???" \
$COMMAND

REQUEST_METHOD=PUT \
PATH_INFO="$PERSON_AC" \
QUERY_STRING="$PERSON_AC_DATA???" \
$COMMAND

# Remove AA
REQUEST_METHOD=DELETE \
PATH_INFO="$PERSON_AA" \
QUERY_STRING= \
$COMMAND

# Search for AC
REQUEST_METHOD=GET \
PATH_INFO="$BASE" \
QUERY_STRING="??sn=Casals?" \
$COMMAND

# Search for AB or AC
REQUEST_METHOD=GET \
PATH_INFO="$BASE" \
QUERY_STRING="??(|(sn=Casals)(sn=Blick))?" \
$COMMAND

# Update AB's phone (emergency number from the IT Crowd)
REQUEST_METHOD=PATCH \
PATH_INFO="$PERSON_AB" \
QUERY_STRING="telephoneNumber=0118 999 881 999 119 7253???" \
$COMMAND

# Remove AC
REQUEST_METHOD=DELETE \
PATH_INFO="$PERSON_AC" \
QUERY_STRING= \
$COMMAND

# Search for AB or AC (only AB is still in the DIT)
REQUEST_METHOD=GET \
PATH_INFO="$BASE" \
QUERY_STRING="??(|(sn=Casals)(sn=Blick))?" \
$COMMAND

# Remove AB
REQUEST_METHOD=DELETE \
PATH_INFO="$PERSON_AB" \
QUERY_STRING= \
$COMMAND

# Any people left? (Yes, in the example data)
REQUEST_METHOD=GET \
PATH_INFO="$BASE" \
QUERY_STRING="??(objectClass=person)?" \
$COMMAND
