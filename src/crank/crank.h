/*
 *   SPDX-FileCopyrightText: 2014, 2015 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2015 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#ifndef STEAMWORKS_CRANK_H
#define STEAMWORKS_CRANK_H

#include "common/verb.h"

#include "3rdparty/flags/flags.hpp"

#include <memory>


enum class RequestType
{
	JSON = 1 << 0,
	HTTP = 1 << 1
};
ALLOW_FLAGS_FOR_ENUM(RequestType)
using RequestTypeFlags = flags::flags<RequestType>;

class CrankDispatcher : public VerbDispatcher
{
private:
	class Private;
	std::unique_ptr<Private> d;

	typedef enum
	{
		disconnected = 0,
		connected,
		stopped
	} State;
	State m_state;

public:
	CrankDispatcher(RequestTypeFlags f);

	int exec(const std::string& verb, const Values& values, Object& response) override;
	int exec(const SteamWorks::HTTP::Request& request, VerbDispatcher::Object& response) override;

	State state() const { return m_state; }

	/** @brief Connect to the given LDAP URI
	 *
	 * This method is for early-connect (connecting at start-up, rather
	 * than waiting for an FCGI message telling the crank to connect).
	 */
	int do_connect(const std::string& uri, const std::string& user, const std::string& password, Object& response);

protected:
	// Generic commands
	int do_connect(const Values& values, Object& response);
	int do_stop(const Values& values);
	int do_serverinfo(Object& response);
	int do_serverstatus(Object& response);

	// LDAP search / update etc.
	int do_search(const std::string& base, const std::string& filter, Object& response);
	int do_search(const Values& values, Object& response);
	int do_update(const std::string& dn, const std::string& attributes, Object& response);
	int do_update(const Values& values, Object& response);
	int do_delete(const std::string& dn, Object& response);
	int do_delete(const Values& values, Object& response);
	int do_add(const std::string& dn, const std::string& attributes, Object& response);
	int do_add(const Values& values, Object& response);

	// Meta-information about the server
	int do_typeinfo(const Values& values, Object& response);
};


#endif
