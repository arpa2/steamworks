/*
 *   SPDX-FileCopyrightText: 2014, 2015 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2015 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#include "crank.h"

#include "common/fcgi.h"
#include "common/logger.h"

#include <getopt.h>

static const char progname[] = "Crank";
static const char version[] = "v0.1";
static const char copyright[] = "Copyright (C) 2014, 2015 InternetWide.org and the ARPA2.net project";

static const char no_protocol[] = "No protocols (json or http) enabled.";
static const char connect_failed[] = "Could not (early) connect.";

static void version_info()
{
	printf("SteamWorks %s %s\n", progname, version);
	printf("%s\n", copyright);
}

static void version_usage()
{
	printf(R"(
Usage:
    crank [-v] [-h]
    crank [-HJ] [-c URI] [-D binddn] [-w secret]

)");
}

static void version_help()
{
	version_info();
	printf(R"(
Use the Crank to handle LDAP access: CRUD operations on LDAP data
through JSON or HTTP requests.

)");
	version_usage();
}

struct ConnectionInfo
{
	std::string uri;
	std::string binddn;
	std::string secret;
};

/** Handle command-line arguments and return @c true if it's ok.
 */
static bool handle_args(int argc, char** argv, RequestTypeFlags& flags, ConnectionInfo& info)
{
	// clang-format off
	const struct option longopts[] = {
		{ "version", no_argument, nullptr, 'v' },
		{ "help", no_argument, nullptr, 'h' },
		{ "http", no_argument, nullptr, 'H' },
		{ "json", no_argument, nullptr, 'J' },
		{ "connect", required_argument, nullptr, 'c' },
		{ "binddn", required_argument, nullptr, 'D' },
		{ "secret", required_argument, nullptr, 'w' },
		{ 0, 0, 0, 0 },
	};
	// clang-format on

	bool carry_on = true;
	int index = 0;
	int iarg = 0;

	while (iarg != -1)
	{
		iarg = getopt_long(argc, argv, "vhHJc:D:w:", longopts, &index);

		switch (iarg)
		{
		case 'J':
			flags |= RequestType::JSON;
			break;
		case 'H':
			flags |= RequestType::HTTP;
			break;
		case 'c':
			info.uri = std::string(optarg);
			break;
		case 'D':
			info.binddn = std::string(optarg);
			break;
		case 'w':
			info.secret = std::string(optarg);
			break;
		case 'v':
			version_info();
			carry_on = false;
			break;
		case 'h':
			version_help();
			carry_on = false;
			break;
		case '?':
			carry_on = false;
			break;
		case -1:
			// End of options, detected next time around
			break;
		default:
			abort();
		}
	}

	return carry_on;
}

static inline SteamWorks::Logging::Logger& myLogger()
{
	return SteamWorks::Logging::getLogger("steamworks.crank");
}

int main(int argc, char** argv)
{
	SteamWorks::Logging::Manager logManager("crank.properties");
	SteamWorks::Logging::getRoot().debugStream() << "SteamWorks Crank " << copyright;

	RequestTypeFlags f(flags::empty_t {});
	ConnectionInfo connect_info;
	if (handle_args(argc, argv, f, connect_info))
	{
		if (!f)
		{
			myLogger().warn(no_protocol);
			std::cerr << no_protocol << std::endl;
			return 1;
		}
		CrankDispatcher* dispatcher = new CrankDispatcher(f);
		SteamWorks::FCGI::init_logging("steamworks.crank.fcgi");
		if (!connect_info.uri.empty())
		{
			VerbDispatcher::Object bogus;
			if (dispatcher->do_connect(connect_info.uri, connect_info.binddn, connect_info.secret, bogus)
			    != 0)
			{
				myLogger().errorStream() << connect_failed << " URI: '" << connect_info.uri << '\'';
				std::cerr << connect_failed << std::endl;
				return 1;
			}
		}
		SteamWorks::FCGI::mainloop(dispatcher);
	}
	return 0;
}
