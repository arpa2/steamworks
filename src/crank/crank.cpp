/*
 *   SPDX-FileCopyrightText: 2014, 2015 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2015 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#include "crank.h"

#include "common/logger.h"
#include "common/request.h"
#include "common/splitstring.h"

#include "swldap/search.h"
#include "swldap/serverinfo.h"
#include "swldap/update.h"

static SteamWorks::Logging::Logger& myLogger()
{
	return SteamWorks::Logging::getLogger("steamworks.crank");
}

/** @brief Inserts HTTP path information in returned JSON
 *
 * Out-of-spec: adds a "$dnpath" attribute to the returned JSON @p response
 * with a HTTP path (string value) to the corresponding dn; this reverses
 * the dn notation to match HTTP paths (see also request.cpp, reversePath()
 *
 * Since "$dnpath" cannot be an attribute name, this should be ignored
 * by "normal" LDAP consumers, but can be used by HTTP clients.
 */
static void add_httpdn(VerbDispatcher::Object& response)
{
	auto& log = myLogger();
	for (auto& k : response)
	{
		const std::string dn = k.first;
		const std::string path = SteamWorks::SplitString(',', dn.c_str()).reverseJoin('/');
		auto& o = k.second.get<VerbDispatcher::Object>();
		o["$dnpath"] = VerbDispatcher::Values(path);
		log.debugStream() << k.first;
	}
}

class CrankDispatcher::Private
{
	friend class CrankDispatcher;

private:
	std::unique_ptr<SteamWorks::LDAP::Connection> connection;
	bool m_enable_json = false;
	bool m_enable_http = false;

public:
	Private(RequestTypeFlags flags)
	    : connection(nullptr)
	    , m_enable_json(flags & RequestType::JSON)
	    , m_enable_http(flags & RequestType::HTTP)
	{
	}

	~Private() {}
};

CrankDispatcher::CrankDispatcher(RequestTypeFlags flags)
    : d(new CrankDispatcher::Private(flags))
    , m_state(disconnected)
{
}

int CrankDispatcher::exec(const std::string& verb, const Values& values, Object& response)
{
	if (!d->m_enable_json)
	{
		return 0;
	}

	auto& log = myLogger();
	log.debugStream() << "Crank verb=" << verb;

	if (verb == "connect")
		return do_connect(values, response);
	else if (verb == "stop")
		return do_stop(values);
	else if (verb == "search")
		return do_search(values, response);
	else if (verb == "typeinfo")
		return do_typeinfo(values, response);
	else if (verb == "update")
		return do_update(values, response);
	else if (verb == "delete")
		return do_delete(values, response);
	else if (verb == "add")
		return do_add(values, response);
	else if (verb == "serverinfo")
		return do_serverinfo(response);
	else if (verb == "serverstatus")
		return do_serverstatus(response);
	else
	{
		log.warnStream() << "Unknown verb=" << '<' << verb << '>';
		std::string s("Unknown verb '");
		s.append(verb);
		s.append("', ignored.");
		SteamWorks::JSON::simple_output(response, 500, s.c_str());
		return 0;
	}
}

int CrankDispatcher::exec(const SteamWorks::HTTP::Request& request, VerbDispatcher::Object& response)
{
	if (!d->m_enable_http)
	{
		return 0;
	}

	using Method = SteamWorks::HTTP::Method;
	auto& log = myLogger();
	log.debugStream() << "Crank HTTP method=" << static_cast<std::underlying_type<Method>::type>(request.method)
			  << " verb=" << request.extensions;

	const std::string& verb(request.extensions);

	if (verb == "serverinfo")
		return do_serverinfo(response);
	else if (verb == "serverstatus")
		return do_serverstatus(response);
	else if (verb.empty() && request.method == Method::Get)
	{
		int r = do_search(request.dn, request.filter, response);
		if (!r)
		{
			add_httpdn(response);
		}
		return r;
	}
	else if (verb.empty() && request.method == Method::Modify && !request.attributes.empty())
		return do_update(request.dn, request.attributes, response);
	else if (verb.empty() && request.method == Method::Create && !request.attributes.empty())
		return do_add(request.dn, request.attributes, response);
	else if (verb.empty() && request.method == Method::Remove)
		return do_delete(request.dn, response);
	else
	{
		log.warnStream() << "Unknown HTTP verb=" << '<' << verb << '>';
		std::string s("Unknown HTTP verb '");
		s.append(verb);
		s.append("', ignored.");
		SteamWorks::JSON::simple_output(response, 500, s.c_str());
		return 500;
	}
}


// Early connect connects directly to the LDAP server URI,
// unlike do_connect() below, returns a useful error code because
// if we can't connect, we want to bail out.
int CrankDispatcher::do_connect(const std::string& uri,
				const std::string& binddn,
				const std::string& secret,
				Object& response)
{
	auto& log = myLogger();
	if (SteamWorks::LDAP::do_connect(d->connection, uri, binddn, secret, response, log))
	{
		m_state = connected;
		return 0;
	}
	return 1;
}

int CrankDispatcher::do_connect(const Values& values, Object& response)
{
	auto& log = myLogger();
	if (SteamWorks::LDAP::do_connect(d->connection, values, response, log))
	{
		m_state = connected;
	}
	// Always return 0 because we don't want the FCGI to stop.
	return 0;
}

int CrankDispatcher::do_stop(const Values& values)
{
	m_state = stopped;
	d->connection.reset(nullptr);
	return -1;
}

void _serverstatus(VerbDispatcher::Object& response, int status, const char* statusname, const char* message)
{
	picojson::value v(statusname);
	response.emplace("status", v);

	if (!message)
	{
		char buffer[64];
		snprintf(buffer, sizeof(buffer), "Unknown status %d", status);
		v = picojson::value(buffer);
		response.emplace("message", v);
	}
	else
	{
		v = picojson::value(message);
		response.emplace("message", v);
	}

	v = picojson::value((double)status);
	response.emplace("_status", v);
}

int CrankDispatcher::do_serverstatus(Object& response)
{
	switch (m_state)
	{
	case disconnected:
		_serverstatus(response, m_state, "disconnected", "The Crank is not connected to a server.");
		break;
	case stopped:
		_serverstatus(
		    response, m_state, "stopped", "The Crank has stopped and is disconnected from the server.");
		break;
	case connected:
		_serverstatus(response, m_state, "connected", "The Crank is connected to the server.");
		break;
	default:
		_serverstatus(response, m_state, "unknown", nullptr);
	}
	return 0;
}

int CrankDispatcher::do_search(const Values& values, Object& response)
{
	std::string base = values.get("base").to_string();
	std::string filter = values.get("filter").to_string();
	return do_search(base, filter, response);
}

int CrankDispatcher::do_search(const std::string& base, const std::string& filter, Object& response)
{
	auto& log = myLogger();

	if (m_state != connected)
	{
		log.debugStream() << "Search on disconnected server.";
		return 0;
	}
	if (filter.empty())
	{
		log.debugStream() << "Search with empty filter discarded.";
		return 0;
	}

	log.debugStream() << "Search parameter base=" << base;
	log.debugStream() << "Search         filter=" << filter;

	// TODO: check authorization for this query
	SteamWorks::LDAP::Search search(base, filter);
	search.execute(*d->connection, &response);
	return 0;
}

int CrankDispatcher::do_typeinfo(const Values& values, Object& response)
{
	auto& log = myLogger();

	if (m_state != connected)
	{
		log.debugStream() << "Typeinfo on disconnected server.";
		return 0;
	}

	SteamWorks::LDAP::TypeInfo search;
	search.execute(*d->connection, &response);
	return 0;
}

// TODO: use an external library to do this correctly
static void decode_html(std::string& value)
{
	std::string::size_type p = std::string::npos;
	while ((p = value.find("%20")) != std::string::npos)
	{
		value.replace(p, 3, " ");
	}
	// Clean up %25 (thats "%") last, to avoid things like %2520 => %20 => " "
	while ((p = value.find("%25")) != std::string::npos)
	{
		value.replace(p, 3, "%");
	}
}

int CrankDispatcher::do_update(const std::string& dn, const std::string& attributes, VerbDispatcher::Object& response)
{
	auto& log = myLogger();
	if (m_state != connected)
	{
		log.debugStream() << "Update on disconnected server.";
		return 0;
	}

	const auto attributeStrings = SteamWorks::SplitString(',', attributes.c_str()).detach();
	using U = SteamWorks::LDAP::Update;
	U::Attributes attr;
	for (const std::string& assignment : attributeStrings)
	{
		auto equalSign = assignment.find('=');
		if (equalSign != std::string::npos && equalSign > 0)
		{
			std::string key = std::string(assignment, 0, equalSign);
			std::string value = std::string(assignment, equalSign + 1, std::string::npos);
			decode_html(value);
			log.debugStream() << "Set attribute " << key << "=" << value;

			auto existingAttr = attr.find(key);
			if (existingAttr == attr.end())
			{
				attr.emplace(key, value);
			}
			else
			{
				U::AttributeValue& v = existingAttr->second;
				switch (v.index())
				{
				case 1:  // single value -> turn it into a 1-element multi
					v.emplace<2>(U::MultiValue({ std::get<U::OneValue>(v) }));
					// FALLTHROUGH (to append the new value)
				case 2:
					std::get<U::MultiValue>(v).emplace_back(value);
					break;
				case 0:
				default:
					v.emplace<1>(value);
				}
			}
		}
	}
	SteamWorks::LDAP::Update u(dn, attr);
	u.execute(*d->connection, &response);
	return 0;
}

int CrankDispatcher::do_update(const Values& values, Object& response)
{
	auto& log = myLogger();

	if (m_state != connected)
	{
		log.debugStream() << "Update on disconnected server.";
		return 0;
	}

	picojson::value v = values.get("values");
	if (!v.is<picojson::array>())
	{
		log.debugStream() << "Update json is not an array of updates.";
		return 0;
	}

	for (unsigned int count = 0;; count++)
	{
		log.debugStream() << "Updating #" << count;

		SteamWorks::LDAP::Update u(v.get(count));
		if (u.is_valid())
		{
			u.execute(*d->connection, &response);
			log.debugStream() << "Completed #" << count;
		}
		else
		{
			break;
		}
	}
	return 0;
}

int CrankDispatcher::do_delete(const Values& values, Object& response)
{
	return do_delete(values.get("dn").to_string(), response);
}

int CrankDispatcher::do_delete(const std::string& dn, Object& response)
{
	auto& log = myLogger();

	if (m_state != connected)
	{
		log.debugStream() << "Update on disconnected server.";
		return 0;
	}

	if (dn.empty())
	{
		log.debugStream() << "Can't delete with an empty DN.";
		// TODO: error in the response object?
		return 0;
	}

	SteamWorks::LDAP::Remove r(dn);
	if (r.is_valid())
	{
		r.execute(*d->connection, &response);
	}
	return 0;
}


int CrankDispatcher::do_add(const Values& values, Object& response)
{
	auto& log = myLogger();

	if (m_state != connected)
	{
		log.debugStream() << "Add on disconnected server.";
		return 0;
	}

	picojson::value v = values.get("values");
	if (!v.is<picojson::array>())
	{
		log.debugStream() << "Add json is not an array of additions.";
		return 0;
	}

	for (unsigned int count = 0;; count++)
	{
		log.debugStream() << "Add #" << count;

		SteamWorks::LDAP::Addition u(v.get(count));
		if (u.is_valid())
		{
			u.execute(*d->connection, &response);
			log.debugStream() << "Completed #" << count;
		}
		else
		{
			break;
		}
	}
	return 0;
}

int CrankDispatcher::do_add(const std::string& dn, const std::string& attributes, Object& response)
{
	auto& log = myLogger();
	if (m_state != connected)
	{
		log.debugStream() << "Add on disconnected server.";
		return 0;
	}

	// An add and an update are almost the same; this code copied out of
	// .. do_update();
	const auto attributeStrings = SteamWorks::SplitString(',', attributes.c_str()).detach();
	SteamWorks::LDAP::Update::Attributes attr;
	for (const std::string& assignment : attributeStrings)
	{
		auto equalSign = assignment.find('=');
		if (equalSign != std::string::npos && equalSign > 0)
		{
			attr.emplace(std::string(assignment, 0, equalSign),
				     std::string(assignment, equalSign + 1, std::string::npos));
		}
	}
	SteamWorks::LDAP::Addition u(dn, attr);
	u.execute(*d->connection, &response);
	return 0;
}


int CrankDispatcher::do_serverinfo(Object& response)
{
	auto& log = myLogger();

	if (m_state != connected)
	{
		log.debugStream() << "ServerInfo on disconnected server.";
		return 0;
	}

	SteamWorks::LDAP::ServerControlInfo info;
	info.execute(*d->connection, &response);

	return 0;
}
