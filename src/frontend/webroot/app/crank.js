/* SPDX-FileCopyrightText: 2021 Adriaan de Groot <groot@kde.org>
 * SPDX-License-Identifier: BSD-2-Clause
 */

// Endpoint of the Crank. This should match the API endpoint
// defined in the lighttpd.conf; the value here matches
// the example documentation.
const crank_server = 'http://localhost:8000/crank/';

// Does a single request to the path (that path includes query-parameters)
// and returns a JSON promise; use .then() on the returned value to
// do further processing.
const crank_request = function(path) {
	console.log('crank request:' + path)
	return window.fetch( crank_server + path)
		.then(response => response.json());
}

// Takes a user object and ensures that the accessControlList is an array
// because if the user has **one** entry, that is returned as a string
// by the crank.
const munge_user = function(user) {
	if (typeof(user.accessControlList) === "string") {
		user.accessControlList = [user.accessControlList]
	}
}

const crank_reload = function(app) {
	crank_request('????serverstatus')
		.then(data => app.status_string = data.message);
	crank_request('ou=InternetWide/o=arpa2.net???objectClass=account')
		.then(data => {
			users = Object.values(data)
			users.forEach(munge_user)
			app.users = users
		} );
}

// Converts an ACL list into a single comma-separated string of
// assignments, for LDAP-HTTP updates.
const munge_acl = function(acllist) {
	s = ""
	acllist.forEach(
		function f(acl) { s = s+ "accessControlList=" + acl + ","});
	return s;
}

const app = Vue.createApp({
	data() {
		return {
			status: false,
			status_string: 'The Crank is in an indeterminate state',
			users: null
		}
	},
	created() {
		crank_reload(this)
	},
	methods: {
		addacl(account) {
			const dn = account.dn;
			var acl = window.prompt("New ACL entry", "prio flags domain");
			if (acl == null || acl == "") {
				console.log("Cancel addacl on " + dn)
			} else {
				console.log("Add acl '" + acl + "' on " + dn)
				// Do the push() on the list locally, so we don't need to re-fetch
				account.accessControlList.push(acl)
				// Note that this **replaces** the existing value, which
				// is why we send all the values again.
				window.fetch(crank_server + account.$dnpath + "?" + munge_acl(account.accessControlList),
							 {method:"PATCH"}).then( /* error reporting? */ );
			}
		},
		dropacl(account, acl) {
			const dn = account.dn;
			const i = account.accessControlList.indexOf(acl);
			if (i>=0) {
				account.accessControlList.splice(i,1);
				window.fetch(crank_server + account.$dnpath + "?" + munge_acl(account.accessControlList),
							 {method:"PATCH"}).then( /* error reporting? */ );
			}
		},
		addaccount(uid, associated, parent) {
				window.fetch(crank_server + "ou=InternetWide/o=arpa2.net/ou=Reservoir/associatedDomain=" + associated + "/uid=" + uid +
				"?objectClass=account"
				,
							 {method:"PUT"}).then( /* error reporting? */ );
		}
	}
})
