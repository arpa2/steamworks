/*
 * SPDX-FileCopyrightText: 2021 Adriaan de Groot <groot@kde.org>
 * SPDX-License-Identifier: BSD-2-Clause
 */

/* This is a klunky load-content-from-elsewhere template system.
 *
 * All class "atemplate" elements are inspected for a "src" attribute
 * and that is unconditionally (and probably unsafely) loaded.
 */

/** @brief Returns a closure to update an element from a promise.
 *
 * Updates @p e with content from a promise.
 */
function update_template_element(e) {
	return function f(r) { e.innerHTML = r; }
}

var templates = document.getElementsByClassName("atemplate");
for (i=0; i < templates.length; i++) {
	var f = update_template_element(templates[i]);
	window.fetch(templates[i].getAttribute("src")).then(response => response.text()).then(f);
}
