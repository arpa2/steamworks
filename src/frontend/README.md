<!-- SPDX-FileCopyrightText: 2021 Adriaan de Groot <groot@kde.org>
     SPDX-License-Identifier: BSD-2-Clause
  -->

> This is a full-fledged "front end" example for the Crank.
> It allows the user to manipulate IDhub users.
> The example assumes that the **sample data** from the
> ARPA2 HOWTO is in use (in particular, that the DN for
> ARPA2 is valid and that the example domains are in use)
> and that the **Crank is running** already.

## Environment

Assumed running:
- LDAP server somewhere with sample data

Assumed available:
- spawn-fcgi
- Crank
- lighttpd
- several terminal windows

### Running the Crank

> See also `src/crank/README.md`

The sample HTML assumes that the Crank is running on
localhost, serving HTML requests, and is already
connected to the LDAP server. A typical command-line
to start the Crank (with the sample LDAP configuration from ARPA2)
looks like this in the `build/` directory:
```
$ LDAP_URI=ldap://db/
$ LDAP_BASE="o=arpa2.net,ou=InternetWide"
$ LDAP_PW="-D $LDAP_BASE -w sekreet"
$ CRANK_PORT=8000
$ spawn-fcgi -p $CRANK_PORT -n ./crank -H -c $LDAP_URI $LDAP_PW
```

### Running the Web Server

There is a file `lighttpd.conf` in this directory which is
pre-configured to serve up the web application and use the
running Crank for LDAP services.

Run lighttpd in this directory with the sample configuration file:
```
lighttpd -D -f lighttpd.conf
```

### Accessing the Web Application

Fetch from the server with a web browser pointed
to the [server at port 3000](http://localhost:3000/crank/ou=InternetWide/o=arpa2.net???objectClass=person?).
This **particular** query will fetch the JSON representation of
the persons in the LDAP tree -- usually three or four, depending
a little on the sample data and what tests have already been done.

The main [Identity application](http://localhost:3000/index.html) offers
a nicer UI than reading-JSON-in-the-browser.

