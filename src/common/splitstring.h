/*
 *   SPDX-FileCopyrightText: 2021 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#ifndef STEAMWORKS_COMMON_SPLITSTRING_H
#define STEAMWORKS_COMMON_SPLITSTRING_H

#include <string>
#include <vector>

namespace SteamWorks
{
struct SplitString
{
	SplitString() {};
	SplitString(char sep, const char* path, int len);
	SplitString(char sep, const char* path);
	/** @brief As above, but with a std::string
	 *
	 * Rather than write the algorithm out again with
	 * std::string and iterators, defer to the char*
	 * approach of scanning a buffer.
	 */
	SplitString(char sep, const std::string& path)
	    : SplitString(sep, path.c_str(), path.length())
	{
	}

	bool isValid() const { return m_len == 0 && (m_len > 0 && m_parts.size() > 0); }

	std::string join(char sep) const;
	std::string join() const { return join(m_sep); }

	std::string reverseJoin(char sep) const;
	std::string reverseJoin() const { return reverseJoin(m_sep); }

	std::vector<std::string> detach() const;

	using PartsVec = std::vector<std::pair<const char*, int>>;
	PartsVec m_parts;
	int m_len = -1;  // Length of original string
	char m_sep = '/';
};
}  // namespace SteamWorks

#endif
