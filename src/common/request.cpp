/*
 * SPDX-FileCopyrightText: 2014-2016 InternetWide.org and the ARPA2.net project
 * SPDX-FileCopyrightText: 2014 Adriaan de Groot <groot@kde.org>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "request.h"

#include "logger.h"
#include "splitstring.h"

#include <string>

#if 0
static inline SteamWorks::Logging::Logger& myLogger()
{
	return SteamWorks::Logging::getLogger("steamworks.fcgi");
}
#endif

void SteamWorks::JSON::simple_output(SteamWorks::JSON::Object& response, int status, const char* message, const int err)
{
	if (status)
	{
		picojson::value status_v(static_cast<double>(status));
		response.emplace(std::string("status"), status_v);
	}
	if ((message != nullptr) && (strlen(message) > 0))
	{
		picojson::value msg_v { std::string(message) };
		response.emplace(std::string("message"), msg_v);
	}
	if (errno)
	{
		picojson::value errno_v { static_cast<double>(err) };
		response.emplace(std::string("errno"), errno_v);
	}
}

SteamWorks::HTTP::Method SteamWorks::HTTP::getMethod(const char* method)
{
	using SteamWorks::HTTP::Method;
	if (!method)
	{
		return Method::None;
	}

	static const char get[] = "GET";
	static const char modify[] = "PATCH";
	static const char create[] = "PUT";
	static const char remove[] = "DELETE";

	if (strncmp(method, get, sizeof(get)) == 0)
	{
		return Method::Get;
	}
	if (strncmp(method, modify, sizeof(modify)) == 0)
	{
		return Method::Modify;
	}
	if (strncmp(method, create, sizeof(create)) == 0)
	{
		return Method::Create;
	}
	if (strncmp(method, remove, sizeof(remove)) == 0)
	{
		return Method::Remove;
	}
	return Method::None;
}

/* Converts a path (guaranteed to not start with '/') in URI-style
 * (`/a/b/c` -- without that leading '/') into dn-style (`c,b,a`).
 */
static inline std::string reversePath(const SteamWorks::SplitString& s)
{
	return s.reverseJoin(',');
}

SteamWorks::HTTP::Request::Request(const char* httpmethod, const char* path, const char* query)
    : dn(reversePath(SplitString('/', path && *path == '/' ? path + 1 : path)))
    , method(getMethod(httpmethod))
{
	const char *q_scope = nullptr, *q_filter = nullptr, *q_extension = nullptr, *bogus = nullptr;

	// Returns a chunk of the string starting at @p begin until the next
	// '?' character, updating @p end to point to the start of the next chunk.
	auto stringify = [](const char* begin, const char*& end)
	{
		end = begin ? strchr(begin, '?') : nullptr;
		if (begin && end)
			return std::string(begin, end++);
		if (begin)
			return std::string(begin);
		return std::string();
	};

	attributes = stringify(query, q_scope);
	scope = stringify(q_scope, q_filter);
	filter = stringify(q_filter, q_extension);
	extensions = stringify(q_extension, bogus);
}
