/*
 *   SPDX-FileCopyrightText: 2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#include "verb.h"

#include "logger.h"

#include <sys/select.h>

static SteamWorks::Logging::Logger& myLogger()
{
	return SteamWorks::Logging::getLogger("steamworks.common");
}

void VerbDispatcher::poll() {}

int VerbDispatcher::exec(const std::string& verb,
			 const VerbDispatcher::Values& values,
			 VerbDispatcher::Object& response)
{
	myLogger().warn("Unsupported request method: JSON");
	return -1;
}

int VerbDispatcher::exec(const SteamWorks::HTTP::Request& request, VerbDispatcher::Object& response)
{
	myLogger().warn("Unsupported request method: HTTP");
	return -1;
}
