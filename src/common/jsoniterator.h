/*
 *   SPDX-FileCopyrightText: 2014-2016 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2016 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

/**
 * A "multi-iterator" across zero or more attributes
 * of a JSON object, which produces tuples for each
 * combination of the attributes-values.
 *
 * Taking each attribute $a$ as a set of attribute-values
 * $A_a$, produces the cartesian product $A_{a1} \cross A_{a2} .. $
 * as a list.
 */
#ifndef STEAMWORKS_COMMON_JSONITERATOR_H
#define STEAMWORKS_COMMON_JSONITERATOR_H

#include "3rdparty/picojson.h"
#include "logger.h"

#include <forward_list>

/** @brief Look up (case-insensitive) the name of an attribute in @p object
 *
 * Objects may have attributes named (case-sensitive) different from the
 * names of variables in Pulley scripts (e.g. because the attributes
 * get lower-cased when transformed to JSON, but the variables in
 * the script are case-sensitive and carry semantic information).
 *
 * Returns the name of the attribute in the @p object, given a variable
 * @p name. Sets @p found to @c true if it is found, @p false otherwise
 * (and returns an empty string in that case).
 *
 * Since it is **possible** to use "" as an attribute name, an
 * empty-string return should still be checked against @p found.
 */
std::string attributeName(const picojson::object& object, const std::string& name, bool& found)
{
	// Look up (case-insensitive) the name actually used in the object
	for (const auto& attrname : object)
	{
		if (strcasecmp(name.c_str(), attrname.first.c_str()) == 0)
		{
			found = true;
			return attrname.first;
		}
	}
	found = false;
	return std::string();
}

class MultiIterator
{
public:
	using inner_value_t = std::string;
	using value_t = std::vector<inner_value_t>;

private:
	using inner_list_t = std::forward_list<inner_value_t>;
	using inner_iterator = inner_list_t::const_iterator;
	using outer_list_t = std::vector<inner_list_t>;
	using outer_iterator = outer_list_t::const_iterator;

	std::vector<bool> m_listy;  // Is the thing in each position list-ish, or a constant value?
	value_t m_constants;
	std::vector<picojson::array::const_iterator> m_begins, m_ends, m_iter;
	size_t m_size;
	bool m_firstly;  // Just in case all dimensions are constants, do at least once.

public:
	MultiIterator(const picojson::object& object, const std::vector<std::string>& names)
	    : m_listy(names.size())
	    , m_constants(names.size())
	    , m_begins(names.size())
	    , m_ends(names.size())
	    , m_iter(names.size())
	    , m_size(names.size())
	    , m_firstly(true)
	{
		unsigned int nameindex = 0;
		for (const auto& f : names)
		{
			bool found = false;
			std::string object_name
			    = attributeName(object, f, found);  // Attribute name (case-insensitive) in object

			if (!found)
			{
				// Not mapped
				m_listy[nameindex] = false;
				m_constants[nameindex] = std::string();
				nameindex++;
				continue;
			}

			const picojson::value& v = object.at(object_name);

			if (v.is<picojson::array>())
			{
				const picojson::array& vv = v.get<picojson::array>();
				m_listy[nameindex] = true;
				// Skip m_constantsp[nameindex]
				m_begins[nameindex] = vv.begin();
				m_iter[nameindex] = vv.begin();
				m_ends[nameindex] = vv.end();
			}
			else
			{
				m_listy[nameindex] = false;
				m_constants[nameindex] = v.get<std::string>();
			}
			nameindex++;
		}
	}

	/** @brief Sets the value of attribute @p index to @p value
	 *
	 * This replaces whatever value was -- if it was a list-valued attribute,
	 * it is now a single value. The @p index refers to the order in which
	 * attributes were named in the vector @c names passed to the constructor.
	 */
	void setConstant(unsigned int index, std::string value)
	{
		if (index < m_size)
		{
			m_listy[index] = false;
			m_constants[index] = std::move(value);
		}
	}

	bool is_done() const
	{
		if (m_firstly)
		{
			return false;
		}

		for (unsigned int i = 0; i < m_size; i++)
		{
			if (m_listy[i] && (m_iter[i] != m_ends[i]))
			{
				return false;
			}
		}
		return true;
	}

	value_t next()
	{
		if (is_done())
		{
			return value_t();
		}

		m_firstly = false;

		value_t v;
		for (unsigned int i = 0; i < m_size; i++)
		{
			if (m_listy[i])
			{
				if (m_iter[i] == m_ends[i])
				{
					m_iter[i] = m_begins[i];
				}
				v.push_back((*m_iter[i]).get<std::string>());
			}
			else
			{
				v.push_back(m_constants[i]);
			}
		}

		for (unsigned int i = 0; i < m_size; i++)
		{
			if (m_listy[i] && (++m_iter[i] != m_ends[i]))
			{
				break;
			}
		}
		return v;
	}
};

#endif
