/*
 *   SPDX-FileCopyrightText: 2014-2015 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2015 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#ifndef STEAMWORKS_COMMON_VERB_H
#define STEAMWORKS_COMMON_VERB_H

#include "request.h"

#include "3rdparty/picojson.h"

class VerbDispatcher
{
public:
	using Values = picojson::value;
	using Object = picojson::value::object;

	/** @brief Handle a single JSON request
	 *
	 * Query parameters, if at all present, are in @p values and the specific
	 * request action ("verb") is lifted from JSON input data as @p verb.
	 * Write the response data for the the request into @p response.
	 *
	 * @return 0 on success
	 * @return > 0 on success with a special HTTP response (e.g. 500)
	 * @return < 0 on failure, which will stop the dispatcher
	 */
	virtual int exec(const std::string& verb, const Values& values, Object& response);
	/* @brief Handle a single HTTP request
	 *
	 * See exec(), above, for details. The request parameters and verb
	 * are stored as fields in @p request.
	 */
	virtual int exec(const SteamWorks::HTTP::Request& request, Object& response);

	/**
	 * If this dispatcher has anything to poll (regardless of
	 * select() on the file-descriptors it might be watching)
	 * then poll that.
	 *
	 * The default implementation does nothing.
	 */
	virtual void poll();
};

#endif
