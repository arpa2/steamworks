/*
 *   SPDX-FileCopyrightText: 2021 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#include "splitstring.h"

#include <cstring>

namespace
{
template <typename IT>
std::string join(int len, char sep, IT&& it, const IT& end)
{
	std::string dn;
	dn.reserve(len);

	while (it != end)
	{
		dn.append((*it).first, (*it).second);
		++it;
		if (it != end)
		{
			dn.append(1, sep);
		}
	}
	return dn;
}
}  // namespace

namespace SteamWorks
{
SplitString::SplitString(char sep, const char* path, int len)
    : m_len(len)
    , m_sep(sep)
{
	const char* start = path;
	int remaining = len;

	while (start && remaining > 0)
	{
		const char* next = strchr(start, sep);
		if (!next)
		{
			m_parts.emplace_back(start, remaining);
			break;
		}
		else
		{
			remaining -= next - start + 1;
			m_parts.emplace_back(start, next - start);
			start = next + 1;
		}
	}
}

SplitString::SplitString(char sep, const char* path)
    : SplitString(sep, path, path ? strlen(path) : 0)
{
}


std::string SplitString::reverseJoin(char sep) const
{
	return ::join(m_len, sep, m_parts.crbegin(), m_parts.crend());
}

std::string SplitString::join(char sep) const
{
	return ::join(m_len, sep, m_parts.cbegin(), m_parts.cend());
}

std::vector<std::string> SplitString::detach() const
{
	std::vector<std::string> s;
	for (const auto& part : m_parts)
	{
		s.emplace_back(part.first, part.second);
	}
	return s;
}


}  // namespace SteamWorks
