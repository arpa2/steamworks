# Defines the two common libraries for SteamWorks:
#	- *swldap* for LDAP handling
#	- *swcommon* for logging, dispatching and JSON handling (utils)

#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright (c) 2014, 2015 InternetWide.org and the ARPA2.net project
#    SPDX-FileCopyrightText: Copyright 2017, 2020, Adriaan de Groot <groot@kde.org>
#

add_library(swcommon STATIC
    fcgi.cpp
    logger.cpp
    request.cpp
    splitstring.cpp
    verb.cpp
)
target_link_libraries(swcommon PUBLIC fcgi)
if(Log4cpp_FOUND)
    target_link_libraries(swcommon PUBLIC Log4cpp::Log4cpp)
endif()

if(Catch2_FOUND)
    add_executable(test_iterator
        test_iterator.cpp)
    target_link_libraries(test_iterator PUBLIC Catch2::Catch2 Log4cpp)
    add_test(NAME test-iterator COMMAND test_iterator)
endif()
