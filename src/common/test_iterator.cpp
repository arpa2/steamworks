/*
 * Tests for data structures common to SteamWorks applications.
 *
 * Coverage is (as of march 2020) rather limited.
 */

/*
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Adriaan de Groot <groot@kde.org>
 */

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "jsoniterator.h"

static const char json_text[] = "{\"a\":\"aap\", \"b\":2, \"c\":\"cow\"}";
static const char json_list[] = R"({
	"a" : [ "aap", "aardvark", "aalscholver" ],
	"c" : [ "cow", "carrot", "conijn" ],
	"b" : 2
})";

picojson::object json_from(const char* text)
{
	picojson::value v;
	std::string json_err = picojson::parse(v, text);
	REQUIRE(json_err == "");

	REQUIRE(!v.is<picojson::null>());
	REQUIRE(v.is<picojson::object>());
	return v.get<picojson::object>();
}

TEST_CASE("JSON Construction", "[json]")
{
	picojson::value v;
	std::string json_err = picojson::parse(v, json_text);
	REQUIRE(json_err == "");

	REQUIRE(!v.is<picojson::null>());
	REQUIRE(v.is<picojson::object>());
	REQUIRE(v.get("bogus").is<picojson::null>());

	auto o = v.get<picojson::object>();
	REQUIRE(!o.empty());
	REQUIRE(o.size() == 3);

	auto b = o["b"];
	REQUIRE(b.is<double>());
	REQUIRE(b.get<double>() == 2);
}

TEST_CASE("JSON Iteration", "[json]")
{
	using Names = std::vector<std::string>;

	picojson::value v;
	std::string json_err = picojson::parse(v, json_text);
	REQUIRE(json_err == "");

	SECTION("Expected Names")
	{
		REQUIRE(!v.is<picojson::null>());
		REQUIRE(v.is<picojson::object>());
		REQUIRE(v.get("bogus").is<picojson::null>());

		auto o = v.get<picojson::object>();
		REQUIRE(!o.empty());
		REQUIRE(o.size() == 3);

		Names n { "a", "c" };
		MultiIterator it(o, n);
		REQUIRE(!it.is_done());

		auto values = it.next();
		REQUIRE(values.size() == 2);
		REQUIRE(values[0] == "aap");
		REQUIRE(values[1] == "cow");
		REQUIRE(it.is_done());

		Names n_rev { "c", "a" };
		MultiIterator it_rev(o, n_rev);
		REQUIRE(!it_rev.is_done());

		values = it_rev.next();
		REQUIRE(values.size() == 2);
		REQUIRE(values[0] == "cow");
		REQUIRE(values[1] == "aap");
		REQUIRE(it_rev.is_done());
	}

	SECTION("Zip")
	{
		auto o = json_from(json_list);
		REQUIRE(!o.empty());
		REQUIRE(o.size() == 3);

		Names n { "a", "c" };
		MultiIterator it(o, n);
		REQUIRE(!it.is_done());

		auto values = it.next();
		REQUIRE(values.size() == 2);
		REQUIRE(values[0] == "aap");
		REQUIRE(values[1] == "cow");
		REQUIRE(!it.is_done());

		values = it.next();
		REQUIRE(values.size() == 2);
		REQUIRE(values[0] == "aardvark");
		REQUIRE(values[1] == "cow");

		values = it.next();
		REQUIRE(values.size() == 2);
		REQUIRE(values[0] == "aalscholver");
		REQUIRE(values[1] == "cow");
		REQUIRE(!it.is_done());

		// Jump to the next value in the second coordinate
		values = it.next();
		REQUIRE(values.size() == 2);
		REQUIRE(values[0] == "aap");
		REQUIRE(values[1] == "carrot");

		// Skip the next 2 in this "row" and all three of the last one,
		// so we've read nine values (3 * 3).
		for (int i = 0; i < 2 + 3; ++i)
		{
			values = it.next();
			REQUIRE(values.size() == 2);
		}
		REQUIRE(it.is_done());
	}

	SECTION("Missing Names")
	{
		auto o = json_from(json_text);
		REQUIRE(!o.empty());
		REQUIRE(o.size() == 3);

		Names n { "a", "d" };  // Note d is missing
		MultiIterator it(o, n);
		REQUIRE(!it.is_done());

		auto values = it.next();
		REQUIRE(values.size() == 2);
		REQUIRE(values[0] == "aap");
		REQUIRE(values[1] == "");

		Names n_rev { "d", "a" };  // Note d is missing
		MultiIterator it_rev(o, n_rev);
		REQUIRE(!it_rev.is_done());

		values = it_rev.next();
		REQUIRE(values.size() == 2);
		REQUIRE(values[0] == "");
		REQUIRE(values[1] == "aap");
	}
}
