/*
 *   SPDX-FileCopyrightText: 2014-2015 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2015 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#include "logger.h"

#ifdef NDEBUG
SteamWorks::Logging::LoggerStream SteamWorks::Logging::Logger::stream;
SteamWorks::Logging::Logger SteamWorks::Logging::Manager::logger;
#else
int SteamWorks::Logging::Manager::instances = 0;
#endif

static char hex[] = "0123456789ABCDEF";

void SteamWorks::Logging::log_hex(SteamWorks::Logging::LoggerStream& log, const uint8_t* p, uint32_t len)
{
	for (uint32_t i = 0; i < len; i++, p++)
	{
		char c0 = hex[(*p & 0xf0) >> 4];
		char c1 = hex[*p & 0x0f];
		log << c0 << c1 << ' ';
	}
}

void SteamWorks::Logging::log_hex(SteamWorks::Logging::LoggerStream& log, const char* p, uint32_t len)
{
	log_hex(log, (const unsigned char*)p, len);
}

void SteamWorks::Logging::log_indent(SteamWorks::Logging::LoggerStream& log, unsigned int indent)
{
	for (unsigned int i = 0; i < indent; i++)
	{
		log << ' ' << ' ';
	}
}

void SteamWorks::Logging::log_json(SteamWorks::Logging::LoggerStream& stream, const picojson::value::object& o)
{
	for (const auto& i : o)
	{
		stream << i.first << " : " << i.second.serialize(true);
	}
}


extern "C" void write_logger(const char* logname, const char* message)
{
	SteamWorks::Logging::getLogger(std::string(logname)).debugStream() << message;
}

extern "C" void warning_logger(const char* logname, const char* message)
{
	SteamWorks::Logging::getLogger(std::string(logname)).warnStream() << message;
}

extern "C" void log_as_debug(const char* pluginname, const char* message)
{
	static std::string logbase("steamworks.pulley.backend.");
	SteamWorks::Logging::getLogger(logbase + std::string(pluginname)).debugStream() << message;
}

extern "C" void log_as_warning(const char* pluginname, const char* message)
{
	static std::string logbase("steamworks.pulley.backend.");
	SteamWorks::Logging::getLogger(logbase + std::string(pluginname)).warnStream() << message;
}
