/*
 * SPDX-FileCopyrightText: 2014-2016 InternetWide.org and the ARPA2.net project
 * SPDX-FileCopyrightText: 2014 Adriaan de Groot <groot@kde.org>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#ifndef STEAMWORKS_COMMON_REQUEST_H
#define STEAMWORKS_COMMON_REQUEST_H

#include "3rdparty/picojson.h"

#include <string>

namespace SteamWorks
{
namespace JSON
{
using Values = picojson::value;
using Object = picojson::value::object;

struct Request
{
	std::string verb;
	Values arguments;
};

/**
 * Produce a simple JSON output with the status code and message.
 * The response is given attributes "status", "message" and "errno".
 *
 * This does not change the HTTP status of the FCGI response.
 *
 * @see simple_output in fcgi.cpp
 */
void simple_output(Object& response, int status, const char* message = nullptr, const int err = 0);

}  // namespace JSON

namespace HTTP
{
enum class Method
{
	None,
	Get,
	Modify,
	Remove,
	Create,
	Post = 255  // POST is a generic method
};
/** Map a http method name (e.g. "GET") to a Method
 *
 * Unknown, unrecognized, wrong-case or nullptr returns @c None
 */
Method getMethod(const char* method);

/** A (structured) LDAP request from a LDAP URI
 *
 * These are used when there are HTTP requests containing a fragmentary
 * LDAP URI. Tools may or may not support this.
 *
 * Conventionally, the *extensions* part of the URI is used to indicate
 * the operation to perform.
 */
struct Request
{
	std::string dn, attributes, scope, filter, extensions;
	Method method = Method::None;

	/** Create an invalid request. */
	Request() {}

	/** Create a request from a HTTP path and query string
	 *
	 * The path sets the dn, while query string contains the four ?-separated
	 * other parts of the LDAP URI. See https://tools.ietf.org/html/rfc2255 ,
	 * where we ignore the schema and hostport part, and take the path of
	 * the HTTP request (that part that ends up at the FCGI endpoint)
	 * as the dn.
	 *
	 * The path is translated from URL-path notation /a/b/c/ into
	 * (reversed) dn-path notation c,b,a.
	 *
	 * TODO: handle %-encoding
	 * TODO: handle dn-injection via path (e.g. `/ou=unit,o=org/`)
	 */
	Request(const char* httpmethod, const char* path, const char* query);

	/** Valid requests
	 *
	 * Which requests are valid is really up to the consumer; this is
	 * only a most basic check.
	 */
	bool isValid() const
	{
		// We need a method, and then either:
		// - no DN at all and an extension (for "special" methods),
		// - a DN and no extension (for "regular" methods).
		return (method != Method::None)
		    && ((method == Method::Get && dn.empty() && !extensions.empty())
			|| (!dn.empty() && extensions.empty()));
	}
};

}  // namespace HTTP
}  // namespace SteamWorks

#endif
