/*
 *   SPDX-FileCopyrightText: 2014-2015 InternetWide.org and the ARPA2.net project
 *   SPDX-FileCopyrightText: 2015 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: BSD-2-Clause
 */

#include "fcgi.h"

#include "fcgiapp.h"
#include "logger.h"
#include "request.h"
#include "verb.h"

#include "3rdparty/picojson.h"

#include <stdlib.h>
#include <string.h>

static const int MAX_STDIN = 65535;
static int request_count = 0;

namespace fcgi
{
static SteamWorks::Logging::Logger* logger = 0;

/**
 * Reads the remainer of an input stream (this seems to be needed if wrapping
 * FCGI streams into C++ streams on some versions of libg++). Currently no-op.
 */
void drain_input(FCGX_Stream* in) {}

class FCGInputStream
{
private:
	FCGX_Stream* input;
	bool eof, valid;
	char current;

public:
	FCGInputStream(FCGX_Stream* s)
	    : input(s)
	    , eof(false)
	    , valid(false)
	{
	}

	FCGInputStream()
	    : input(0)
	    , eof(true)
	    , valid(false)
	{
	}

	char operator*()
	{
		if (valid)
		{
			return current;
		}
		else
		{
			current = FCGX_GetChar(input);
			if (current < 0)
			{
				eof = true;
			}
			valid = true;
		}
		return current;
	}

	bool operator==(const FCGInputStream& rhs) const
	{
		if (eof && rhs.eof)
		{
			return true;
		}
		else
		{
			return (eof == rhs.eof) && (input == rhs.input);
		}
	}

	// Dummy prefix-++
	FCGInputStream& operator++()
	{
		valid = false;
		return *this;
	}
};

const char _empty_response[] = "Content-type: text/json\r\nContent-length: 2\r\n\r\n{}";

void simple_output(FCGX_Stream* out, int status, const picojson::value& map)
{
	std::string text = map.serialize(true);
	if (text.empty())
	{
		FCGX_PutS(_empty_response, out);
		return;
	}
	const char* const textp = text.c_str();
	if (!textp)
	{
		FCGX_PutS(_empty_response, out);
		return;
	}
	size_t len = strlen(textp);
	if (len < 1)
	{
		FCGX_PutS(_empty_response, out);
		return;
	}

	FCGX_SetExitStatus(status, out);
	FCGX_FPrintF(out, "Content-type: text/json\r\nContent-length: %u\r\n\r\n", len);
	FCGX_PutS(textp, out);
}

void simple_output(FCGX_Stream* out, int status, const char* message = nullptr, const int err = 0)
{
	if (logger && (status != 200))
	{
		logger->debugStream() << "HTTP status " << status << ": " << message;
	}

	picojson::value::object map;
	SteamWorks::JSON::simple_output(map, status, message, err);
	simple_output(out, status, picojson::value(map));
}

static int find_verb(const picojson::value& v, std::string& out)
{
	if (!v.is<picojson::object>())
	{
		return -1;
	}
	const picojson::value::object& obj = v.get<picojson::object>();
	for (picojson::value::object::const_iterator i = obj.begin(); i != obj.end(); ++i)
	{
		if (i->first == "verb")
		{
			out = i->second.to_str();
			return 0;
		}
	}
	return -2;
}

/** Process incoming request as JSON, returns 0 to continue, -1 to stop.
 *
 * Only returns -1 when the @p dispatcher returns != 0
 */
static int handle_json_request(FCGX_Stream* in,
			       FCGX_Stream* out,
			       FCGX_Stream* err,
			       FCGX_ParamArray env,
			       VerbDispatcher* dispatcher)
{
	const char* s_content_length = FCGX_GetParam("CONTENT_LENGTH", env);
	if (!s_content_length)
	{
		simple_output(out, 500, "No request data.");
		return 0;  // Handled correctly, even though we sent an error to the client
	}

	int content_length = atoi(s_content_length);
	if (content_length < 1)
	{
		simple_output(out, 500, "Request data too short.");
		return 0;
	}
	if (content_length > MAX_STDIN)
	{
		content_length = MAX_STDIN;
	}

	picojson::value request_values;
	picojson::value::object response_values;
	std::string error_string;
	std::string verb;
	int r;
	picojson::parse(request_values, FCGInputStream(in), FCGInputStream(), &error_string);
	if (!error_string.empty())
	{
		simple_output(out, 500, error_string.c_str());
		return 0;
	}
	else
	{
		if ((r = find_verb(request_values, verb)) < 0)
		{
			simple_output(out, 500, "No verb.");
			return 0;  // Continue serving, though
		}
		else
		{
			if (logger)
			{
				logger->debug("Got verb '%s'.", verb.c_str());
			}
			if (dispatcher)
			{
				r = dispatcher->exec(verb, request_values, response_values);
			}
			if (r < 0)
			{
				simple_output(out, 500, "Bad request", -r);
				return r;
			}
			else
			{
				picojson::value v(response_values);
				simple_output(out, r ? r : 200, v);
				r = 0;  // Keep serving
			}
		}
	}
	return 0;
}

static int handle_http_request(FCGX_Stream* out, FCGX_ParamArray env, VerbDispatcher* dispatcher)
{
	const char* s_request_method = FCGX_GetParam("REQUEST_METHOD", env);
	const char* s_path_info = FCGX_GetParam("PATH_INFO", env);
	const char* s_query_string = FCGX_GetParam("QUERY_STRING", env);

	// Ensure neither is null
	if (s_path_info == nullptr)
	{
		s_path_info = "";
	}
	if (s_query_string == nullptr)
	{
		s_query_string = "";
	}

	picojson::value request_values;
	picojson::value::object response_values;
	std::string error_string;
	int r = 0;

	SteamWorks::HTTP::Request l(s_request_method, s_path_info, s_query_string);
	if (logger)
	{
		logger->debug("path=%s", l.dn.c_str());
		logger->debug("  a=%s s=%s f=%s x=%s",
			      l.attributes.c_str(),
			      l.scope.c_str(),
			      l.filter.c_str(),
			      l.extensions.c_str());
	}

	if (l.isValid())
	{
		if (logger)
		{
			logger->debugStream() << "verb=" << l.extensions;
		}
		r = dispatcher->exec(l, response_values);
		if (r < 0)
		{
			simple_output(out, 500, "Bad request", -r);
		}
		else
		{
			picojson::value v(response_values);
			simple_output(out, r ? r : 200, v);
			r = 0;  // Keep serving
		}
	}
	else
	{
		simple_output(out, 500, "Invalid HTTP request.");
	}
	return r;
}

static void log_request(const FCGX_ParamArray& env)
{
	if (!logger)
	{
		return;
	}

	auto show = [&env](const char* key)
	{
		const char* v = FCGX_GetParam(key, env);
		logger->debugStream() << key << '=' << (v ? v : "(null)");
	};
	// Keys defined in rfc3875; we only use the interesting ones
	static const char* keys[] = {
		// "AUTH_TYPE" ,
		"CONTENT_LENGTH",
		"CONTENT_TYPE",
		// "GATEWAY_INTERFACE" ,
		"PATH_INFO",
		// "PATH_TRANSLATED" ,
		"QUERY_STRING",
		// "REMOTE_ADDR" ,
		// "REMOTE_HOST" ,
		// "REMOTE_IDENT" ,
		// "REMOTE_USER" ,
		"REQUEST_METHOD",
		"SCRIPT_NAME",
		// "SERVER_NAME" ,
		// "SERVER_PORT" ,
		// "SERVER_PROTOCOL" ,
		// "SERVER_SOFTWARE"
	};
	for (const auto* k : keys)
	{
		show(k);
	}
}

static bool is_http(const char* method)
{
	return SteamWorks::HTTP::getMethod(method) != SteamWorks::HTTP::Method::None;
}

int handle_request(FCGX_Stream* in, FCGX_Stream* out, FCGX_Stream* err, FCGX_ParamArray env, VerbDispatcher* dispatcher)
{
	log_request(env);

	const char* s_request_method = FCGX_GetParam("REQUEST_METHOD", env);

	int r = 0;
	if (is_http(s_request_method))
	{
		r = handle_http_request(out, env, dispatcher);
	}
	else
	{
		r = handle_json_request(in, out, err, env, dispatcher);
	}
	drain_input(in);
	return r;
}

}  // namespace fcgi

int SteamWorks::FCGI::init_logging(const std::string& logname)
{
	SteamWorks::Logging::Logger& log = SteamWorks::Logging::getLogger(logname);
	fcgi::logger = &log;
	return 0;
}

// TODO: integrate with other main loops
int SteamWorks::FCGI::mainloop(VerbDispatcher* dispatcher)
{
	FCGX_Stream *in, *out, *err;
	FCGX_ParamArray envp;

	int r;
	fd_set rfds;
	struct timeval tv;

	/*
	 * The loop-structure is twofold here:
	 *  - receive an FCGI request and process it (blocking)
	 *  - poll for an LDAP update (short timeout)
	 *  - select() for another FCGI request
	 *  - if no FCGI request, continue polling LDAP
	 */
	do
	{
		r = FCGX_Accept(&in, &out, &err, &envp);
		if (r < 0)
			return r;

		r = fcgi::handle_request(in, out, err, envp, dispatcher);
		if (r)
			return r;

		request_count++;
		FCGX_Finish();

		do
		{
			dispatcher->poll();  // This has a (short) timeout

			FD_ZERO(&rfds);
			if (!::FCGX_FD_SET(&rfds))
			{
				if (fcgi::logger)
				{
					fcgi::logger->debugStream() << "No FCGI socket, exiting.";
				}
				r = -1;  // For outer loop
				break;
			}

			/* This spams the debug log with useless select() messages.
			if (fcgi::logger)
			{
				fcgi::logger->debugStream() << "Doing select() for FCGI";
			}
			*/
			tv.tv_sec = 0;
			tv.tv_usec = 10000;
			r = ::select(
			    FD_SETSIZE, &rfds, nullptr, nullptr, &tv);  // Short timeout, just check for request
			// r is the number of ready FDs, or -1 on error.
			// We expect 0 (no FCGI request ready) or 1 (FCGI request ready).
		} while (r == 0);
	} while (r >= 0);

	return 0;
}
