<!-- SPDX-FileCopyrightText: 2015 Adriaan de Groot <groot@kde.org>
     SPDX-License-Identifier: BSD-2-Clause
  -->

This repository holds notes and code for the initial implementation
of parts of the SteamWorks system, in particular the Crank, Shaft and
Pulley components of that system.

    docs/                Holds project documentation
    src/                 Project sources
    src/3rdparty         Third-party imported sources
    src/common           Support code, common components
    src/crank            SteamWorks Crank component
    src/pulley           SteamWorks Pulley component
    src/shaft            SteamWorks Shaft component
    src/frontend         Sample front-end web application for Crank

See LICENSE for overall licensing information, and a README in each
subdirectory for local details. For a quick start you need to go 
through the steps of a typical CMake-based build (see below).

The documentation is written in MarkDown. There is a docs/Makefile that
will produce nicer output using ronn(1).

For example usage of the system, see the examples/ directories in the
various components directories. These contain example data and scenario's
to demonstrate the system; you will need to read some of the documentation
in docs/ to know what to do with them.

## Building (quick-start)

SteamWorks uses [CMake](https://cmake.org/) to build; you will need at least
version 3.13.

Like all ARPA2 (sub-)projects, SteamWorks has a three-stage build:
 - run `cmake` with suitable arguments
 - run the build-system that is generated
 - run the install step that is generated
 
For Windows, this is probably different (e.g. all contained within Visual
Studio. On UNIX-like systems, to spell it out,

```
mkdir build
cd build
cmake ..
make
make install
```

This creates a separate build directory (inside the source checkout; you
can also do it somewhere else entirely), runs `cmake` telling it where
the sources are. The default generator for CMake is Makefiles, so
then `make` is the tool to do the build and install steps.

At the `cmake` step, you may get complains about missing dependencies.
In that case, resolve them first before tying to build the software.


## Dependencies

SteamWorks uses CMake, is written in C++, and is part of the ARPA2
stack, so it uses ARPA2CM -- a collection of CMake modules for
the project. Other dependencies are vaguely described in this
section, but are necessarily OS and distro-dependent. When in
doubt, run `cmake`, which will tell you about the dependencies.

### Linux

This SteamWorks software is compatible with Linux and has been
tested on OpenSUSE 13.2 and Leap 42. In order to build SteamWorks,
you will need to install the following packages:

 - Debian Jesse
   - Packages (use apt-get install)
     - make cmake
     - g++
     - libldap-dev
     - flex bison
     - libsqlite3-dev
- Ubuntu & Derivatives
   - Packages (use apt-get install)
     - All the Debian packages, above
     - liblog4cpp5-dev
 - OpenSUSE (incomplete)
   - Packages (use zypper install)
     - gcc-c++
     - openldap-client, openldap-client-devel
     - sqlite3-devel
   - From source:
     - log4cpp, see http://log4cpp.sourceforge.net/ .
       This is optional.

SteamWorks builds a bundled copy of FCGI, so fcgi-devkit is not needed.

### FreeBSD

This SteamWorks software is compatible with FreeBSD and has been
tested on FreeBSD 10, 12 and 13-STABLE. In order to build
SteamWorks locally, you will need the following packages installed:

 - log4cpp
 - openldap-client
 - sqlite3

SteamWorks builds with both clang (default on the tested FreeBSD
versions) and gcc.

SteamWorks builds a bundled copy of FCGI, so fcgi-devkit is not needed.


## Development

This section describes which settings to use in a variety of IDEs
for building and developing SteamWorks.

### Source Code Settings

SteamWorks code is written in the following style, which is
close to the KDE Frameworks coding style. The style applies to
all C++ code from SteamWorks, so:

 - src/3rdparty is excluded (these contain verbatime copies of
   third-party code which have other style-guides),
 - src/pulley/pulleyscript/ is excluded (this is largely a C
   library, using Rick's coding style).

Style is as follows:
 - tabs for indent, spaces for alignment
 - tabs at 8
 - lines suggested maximum length 78
 - declare type* var, not type *var
 - spaces around operators, after commas
 - { } on lines of their own except in one-line const getter-methods

For new code, the overriding guideline is "follow what's in the
file(s) already and blend in."

There is a `.clang-format` file that does the right thing for code
formatting, and an `.editorconfig` that ought to tell your IDE
things about indentation.

### KDevelop

To use KDevelop as IDE while working on SteamWorks, do the following:
 1- Run make(1) once in the toplevel, to create a build/ directory
    and do initial configuration.
 2- Select Open/Import an existing project.
 3- Select the src/ subdirectory as project directory, import from
    CMakeLists.txt.
 4- As a build directory, select the build/ subdirectory one level
    higher. KDevelop will suggest src/build, but you've already
    created build/ in step 1.

