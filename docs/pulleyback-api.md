<!-- SPDX-FileCopyrightText: 2016 Rick van Rein <rick@openfortress.nl>
     SPDX-License-Identifier: BSD-2-Clause
  -->

> The Pulley backend API is documented in `pulleyback.h`, which is
> the header that provides the API definitions.

